\input{header}

\chapter{Build and Installation}
\label{chp:installation}
\opal and all its flavors are based on several packages which are all installable using \texttt{cmake} or the configure-make-install trilogy.

\opal is also preinstalled on several HPC clusters including the Merlin cluster at PSI. The preinstalled version can be accessed
using the module command:
\begin{footnotesize}
\begin{example}
module load OPAL/1.6.0
\end{example}
\end{footnotesize}

\section{Using pre-build Binaries}
Pre-build binaries are available for SL Linux  and Mac OS X at the following download page: \url{https://amas.psi.ch/OPAL/wiki/Downloads}.

\section{Enabling the Iterative Solvers for Space-Charge Calculation}

Iterative solvers are used to solve the Poisson's equation for the electrostatic potential
on three-dimensional domains \cite{Adelmann:2009p543}.
The multigrid preconditioner and iterative solver are implemented using Trilinos framework. It is essential to install
Trilinos software framework to use the iterative solvers in \opal.

Trilinos is also preinstalled on several HPC clusters including the Merlin cluster at PSI. The preinstalled version can be accessed
using the module command:
\begin{footnotesize}
\begin{example}
module load trilinos
\end{example}
\end{footnotesize}

{\bf Please note:} The Multigrid space charge solver is not yet capable of emitting a beam from the cathode.  You are advised to use the FFT space charge solver for studies with particle emission.

\subsubsection{Trilinos Package}
If Trilinos version ($>$11.0) is not available on your system, download it from the Trilinos webpage
{\url {http://trilinos.sandia.gov/download}}
\footnote{\url{http://trilinos.sandia.gov}}. CMake build system is used to configure, build and install
Trilinos. Instructions for building Trilinos is available in \\
{\url {http://trilinos.sandia.gov/TrilinosBuildQuickRef.html}}

To build {\opal} with {SAAMG\_SOLVER}, Trilinos packages, which are needed to be enabled are:
\begin{itemize}
  \item epetra and epetraext
  \item ml and ml\_parmetis3x
  \item amesos and amesos-superludist
  \item ifpack
  \item teuchos and teuchos-extended
  \item aztecco and aztecoo-teuchos
  \item galeri
  \item belos
\end{itemize}
and Third-Party Library (TPL) support
 \begin{itemize}
  \item ParMETIS (\texttt{-D TPL\_ENABLE\_ParMETIS:BOOL=ON})
  \item BLAS  (\texttt{-D TPL\_ENABLE\_BLAS:BOOL=ON})
  \item LAPACK (\texttt{-D TPL\_ENABLE\_LAPACK:BOOL=ON})
  \item MPI (\texttt{-D TPL\_ENABLE\_MPI:BOOL=ON })
\end{itemize}
To enable a given TPL,  the path to this package's header include and library directories should be specified
when building Trilinos. The TPL libraries such as \filename{libparmetis.a libmetis.a libblas.a liblapacke.a liblapack.a} are needed
to use MultiGrid (SAAMG).

\subsection{Environment Variables}
\label{ssec:envvar_MG}

Assuming Trilinos package is installed in \texttt{\$TRILINOS\_PREFIX} then the following environment variables must be set accordingly.

\texttt{TRILINOS\_DIR} should point the path to the base installation directory of Trilinos

\texttt{TRILINOS\_INCLUDE\_DIR} should point the path to Trilinos' include directory

\texttt{TRILINOS\_LIBRARY\_DIR} should point the path to Trilinos' library directory

If you are using preinstalled version of Trilinos on Merlin cluster at PSI, the environment variables
are automatically set so you don't have to set these yourself.

\begin{footnotesize}
\begin{example}
export TRILINOS_DIR=$TRILINOS_PREFIX
export TRILINOS_INCLUDE_DIR=$TRILINOS_DIR/include
export TRILINOS_LIBRARY_DIR=$TRILINOS_DIR/lib
\end{example}
\end{footnotesize}

\subsection{Build \opal with SAAMG\_SOLVER enabled}
You can enable the solver with\\ \texttt{-DENABLE\_SAAMG\_SOLVER=TRUE} using cmake
and make sure the environment variables \ssecref{envvar_MG}
are set up already before {\opal} installation.

\begin{footnotesize}
\begin{example}
cd $OPAL_ROOT
mkdir build
cd build
cmake -DENABLE_SAAMG_SOLVER=TRUE $OPAL_ROOT
make
\end{example}
\end{footnotesize}

\subsection{Build \opal with AMR\_SOLVER enabled}
{\bf AMReX} \footnote{\url{https://ccse.lbl.gov/AMReX}} software framework is used in \opal\  to add the adaptive mesh
refinement (AMR) technique. You can enable AMR solver with {\tt -DENABLE\_AMR\_SOLVER=TRUE} using cmake.

\begin{footnotesize}
\begin{example}
cd $OPAL_ROOT
mkdir build
cd build
cmake -DENABLE_AMR_SOLVER=TRUE $OPAL_ROOT
make
\end{example}
\end{footnotesize}
Please make sure the following environment variables are set:
\begin{footnotesize}
\begin{example}
export AMREX_HOME=/path/to/target/directory
export AMREX_PREFIX=$AMREX_HOME
export AMREX_INCLUDE_DIR=$AMREX_HOME/include
export AMREX_LIBRARY_DIR=$AMREX_HOME/lib
export AMREX_PERL_DIR=$AMREX_HOME/perl
\end{example}
 \label{subsec:envvar_AMR}
\end{footnotesize}

\subsubsection{AMReX Package}
\label{ssubsection:AMRexBuild}

You can download AMReX from the GitHub server by typing the following commands
\begin{footnotesize}
\begin{example}
cd $HOME/git
git clone https://github.com/AMReX-Codes/amrex.git
\end{example}
\end{footnotesize}

AMReX is Fortran based with extensions to C++.\ It runs in parallel using MPI.\ Please check the following environment variable
before start building the library.
\begin{example}
export AMREX_INSTALL_PREFIX=/path/to/target/directory
\end{example}
Build and install as follows:
\begin{footnotesize}
\begin{example}
cd amrex
mkdir build
cd build
CXX=$MPICXX CC=$MPICC cmake -DBL_SPACEDIM=3 -DENABLE_MPI=1 -DENABLE_BACKTRACE=0
                            -DENABLE_PROFILING=0 -DBL_DEBUG=0 -DENABLE_OpenMP=0
                            -DBL_USE_PARTICLES=1 -DBL_PRECISION=DOUBLE
                            -DCMAKE_INSTALL_PREFIX=$AMREX_INSTALL_PREFIX ..
make -j4
make install    // depending on target directory root access might be required
\end{example}
\end{footnotesize}
Now you are ready to build \opal with AMR support.

\section{Debug Flags}\label{sec:debugflags}

\begin{table}[ht]\footnotesize
  \begin{center}
    \caption{Debug flags.}
    \label{tbl:debug_flags}
      \begin{tabular}{lll}
        \hline
        \tabhead{Name & Description & Default}
        \hline
        DBG\_SCALARFIELD & dumps scalar potential on the grid & not set \\
        DBG\_STENCIL & dumps stencil (SAAMG solver) to a Matlab readable file & not set \\
        \hline
      \end{tabular}
    \end{center}
\end{table}

\paragraph{DBG\_SCALARFIELD} dumps the field to a file called rho\_scalar. The structure of the data can be deduced from the following Matlab script:

\begin{footnotesize}
\begin{example}
function scalfield(RHO)

rhosize=size(RHO)
for i=1:rhosize(1)
  x = RHO(i,1);
  y = RHO(i,2);
  z = RHO(i,3);
  rhoyz(y,z) = RHO(i,4);
  rhoxy(x,y) = RHO(i,4);
  rhoxz(x,z) = RHO(i,4);
  rho(x,y,z) = RHO(i,4);
end
\end{example}
\end{footnotesize}

\paragraph{DBG\_STENCIL} dumps the discretization stencil to a file (A.dat). The following Matlab code will read and store the sparse matrix in the variable 'A'.

\begin{footnotesize}
\begin{example}
load A.dat;
A = spconvert(A);
\end{example}
\end{footnotesize}

\section{\opal~as a Library}
An \opal~library can be build by specifying \texttt{-DBUILD\_LIBOPAL} in the cmake process. The \opal~library is currently used in the opt-pilot, a multi-objective
optimization package \cite{bib:optpilot1}.

\section{Emacs Mode for \opal}
An opal-mode for emacs is provided to get highlighted input files. To use it the user should make the directory \filename{\$HOME/.emacs.d/opal} and copy the file \filename{opal.el} there. Then the following lines
\begin{footnotesize}
\begin{example}
(add-to-list 'load-path "~/.emacs.d/opal")
(autoload 'opal-mode "opal.el" "Enter opal mode" t)
(setq auto-mode-alist (append '(("\\.opal\$" . opal-mode)) auto-mode-alist))
(setq auto-mode-alist (append '(("\\.in\$" . opal-mode)) auto-mode-alist))
\end{example}
\end{footnotesize}
should be added to the emacs configuration file \filename{\$HOME/.emacs}. In case your input file has the extensions \filename{.opal} or \filename{.in}  you will enjoy highlighted
keywords, constants etc.

\section{Regression tests}
The regression rest repository can be found at \url{git@gitlab.psi.ch:OPAL/regression-tests.git}. Several test can be found which are
run on every source code change in order to check the validity of the current version of \opal. This is a good starting-point to learn how to
model accelerators with the various flavours of \opal.

\section{Build unit tests} \label{chp:unittest}
Some portions of \opal can be tested using \emph{unit tests}. Unit tests are implemented to ensure that a particular unit e.g. a function or class works as intended. \opal uses the google testing \emph{gtest} framework to support unit tests. Documentation can be found at \url{https://code.google.com/p/googletest/w/list}.

The \opal unit tests are by default not built. To build the unit tests, first gtest must be installed.

\begin{example}
cd ${OPAL_ROOT}/tests/tools
bash install_gtest.bash
\end{example}

This will check that you have the required packages to build gtest, download the gtest source code from the google website and attempt to build it.

Next modify the cmake files to flag that the unit tests need to be built.
\begin{example}
cd ${OPAL_BUILD_ROOT}/
cmake -DBUILD_OPAL_UNIT_TESTS=1
make
\end{example}
If all goes well, the unit tests should build. To execute the tests, do
\begin{example}
cd ${OPAL_BUILD_ROOT}/
./tests/opal_unit_tests
\end{example}
You should get output that looks like e.g.
\begin{example}
[==========] Running 47 tests from 8 test cases.
[----------] Global test environment set-up.
[----------] 6 tests from OpalRingSectionTest
[ RUN      ] OpalRingSectionTest.TestConstructDestruct
[       OK ] OpalRingSectionTest.TestConstructDestruct (0 ms)
<snip>
[ RUN      ] PolynomialTimeDependenceTest.TDMapNameLookupTest
[       OK ] PolynomialTimeDependenceTest.TDMapNameLookupTest (0 ms)
[----------] 3 tests from PolynomialTimeDependenceTest (0 ms total)

[----------] Global test environment tear-down
[==========] 47 tests from 8 test cases ran. (25 ms total)
[  PASSED  ] 47 tests.

  YOU HAVE 1 DISABLED TEST
\end{example}
If the output does not end in PASSED then something failed. Review the test output to find the test failure.

\subsubsection{Adding a unit test}
To add a unit test to the \opal framework, either edit an existing file or add a new file. In general, there should be one unit test per pair of header, source files. The directory structure reflects \opal directory structure. Source code that is in \verb|${OPAL_ROOT}/classic/5.0/src/<dir>/<Filename.h,cpp>| should have corresponding unit test files in \verb|${OPAL_ROOT}/src/unit_tests/classic_src/<dir>/<FilenameTest.cpp>|. Source code that is in \verb|${OPAL_ROOT}/src/<dir>/<Filename.h,cpp>| should have corresponding unit test files in \verb|${OPAL_ROOT}/src/unit_tests/opal_src/<dir>/<FilenameTest.cpp>|. To update the list of files used by cmake, do
\begin{example}
touch ${OPAL_ROOT}/CMakeLists.txt
\end{example}
Then run \verb|make| as usual.

\input{footer}