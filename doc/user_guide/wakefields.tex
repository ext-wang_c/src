\input{header}

\chapter{Wakefields}
\label{chp:wakefields}
\index{Wakefields}
\TODO{rewrite!}

\opalt provides methods to compute CSR and short-range geometric wakefields.

\section{Geometric Wakefields}
\label{sec:wakefields:shortrange}
Basically there are two different kind of wakefields that can be used. The first one is the wakefield of a round, metallic beam pipe that can be calculated numerically (see Sections~\ref{sec:wakefield} - \ref{sec:TAU}).
Since this also limits the applications of wakefields we also provide a way to import a discretized wakefield from a file %\seesec{WFNAME}.

The wakefield of a round, metallic beam pipe with radius $a$ can be calculated by inverse FFT of the beam pipe impedance. There are known models for beam pipes with \keyword{DC} and \keyword{AC} conductivity. The \keyword{DC} conductivity of a metal is given by
%
\begin{equation}
\sigma_{DC} = \frac{ne^2\tau}{m} \label{eq:dc_cond}
\end{equation}
%
with $n$ the density of conduction electrons with charge $e$, $\tau$ the relaxation time, and $m$ the electron mass. The \keyword{AC} conductivity, a response to applied oscillation fields, is given by
%
\begin{equation}
\sigma_{AC} = \frac{\sigma_{DC}}{1-i\omega\tau} \label{eq:ac_cond}
\end{equation}
%
with $\omega$ denoting the frequency of the fields.

The longitudinal impedance with \keyword{DC} conductivity is given by
%
\begin{equation}  \label{eq:Z[2]}
 Z_{Ldc}(k) = \dfrac{1}{ca} \dfrac{2}{\frac{\lambda}{k}-\frac{ika}{2}}
\end{equation}
%
where
\begin{equation}
\lambda=\sqrt{\dfrac{2\pi\sigma \vert k\vert}{c}}(i+sign(k))
\end{equation}
%
with $c$ denoting the speed of light and $k$ the wave number.

The longitudinal wake can be obtained by an inverse Fourier transformation of the impedance. Since $Re(Z_L(k))$ drops at high frequencies faster than $Im(Z_L(k))$ the cosine transformation can be used to calculate the wake. The following equation holds in both, the \keyword{DC} and \keyword{AC}, case
%
\begin{equation} \label{eq:Calc_Wl}
W_L(s)=10^{-12} \dfrac{2c}{\pi}Re\left(\int_0^\infty Re(Z_L(k))\cos (ks)dk\right)
\end{equation}
%
with $Z_L(k)$ either representing $Z_{L_{DC}}(k)$ or $Z_{L_{AC}}(k)$ depending on the conductivity. With help of the Panofsky-Wenzel theorem
%
\begin{equation}
Z_L(k) = \frac{k}{c}Z_T(k).
\end{equation}
%
we can deduce the transverse wakefield from \eqnref{Calc_Wl}:
%
\begin{equation} \label{eq:Calc_Wt}
W_T(s)= 10^{-12} \dfrac{2c}{\pi}Re\left(\int_0^\infty Re( \frac{c}{k}Z_L(k))\cos (ks)dk\right).
\end{equation}

%\begin{figure}[ht]
%\begin{center}
%\includegraphics[width=0.45\textwidth]{wakeComp/lo_Integration.png}
%\includegraphics[width=0.45\textwidth]{wakeComp/tr_Integration.png}
%\caption{Parameters of the numerical integration scheme: $N$ and $\Delta k$. \textsc{Left:} longitudinal and \textsc{Right:} transversal wake. \label{fig:lo_calc} }
%\end{center}
%\end{figure}

%As shown in \eqnref{Calc_Wl,Calc_Wt} we need to integrate a function to calculate one sampling point of the wakefunction. For the numerical integration \eqnref{Calc_Wl} can be written as a sum from $0$ to $N-1$ since $\infty$ can not be covered numerically, and a discrete $\Delta k$ instead of a infinitesimal $dk$:
%\begin{equation}\label{eq:Num_Calc_Wl}
%W_{L}(s)=10^{-12} \dfrac{2c}{\pi}Re\left(\sum_{i=0}^{N-1} Re(Z_{L}(i \Delta k))\cos (i\Delta ks)dk\right)
%\end{equation}
%with $Re(Z_{L})$ either $Z_{Ldc}$ or $Z_{Lac}$.
%The same holds for \eqnref{Calc_Wt} which is then written as
%\begin{equation}\label{eq:Num_Calc_Wt}
%W_{T}(s)=10^{-12} \dfrac{2c}{\pi}Re\left(\sum_{i=0}^{N-1} Re(\frac{c}{k} Z_{L}(i \Delta k))\cos (i\Delta ks)dk\right).
%\end{equation}
%To have reliable results out of \eqnref{Num_Calc_Wl,Num_Calc_Wt} $N$ the number of sampling points must be big enough and $\Delta k$ the mesh size small enough.
%\figref{lo_calc} trites to give an intuition how $N$ and $\Delta k$ should be chosen. In the figure the parameters are shwn for a round, copper beam pipe with radius $5$\,mm. In \figref{lo_calc}  $Re(Z_{Ldc}(k))\cos (ks)$ with $s$=120 $\mu$m is plotted with respect to k. This equation is integrated in \eqnref{Num_Calc_Wl} to get the longitudinal wakefield. There it is obvious that k must be at least in the order of $10^5$. In \figref{tr_calc} $Re(\frac{c}{k}Z_{Lac}(k))\cos (ks)$ with $s$=120 $\mu$m is plotted with respect to k. This equation is integrated according to \eqnref{Num_Calc_Wt} to get the transversal wakefield. Figure shows a singularity at k=0. To integrate such functions a small $\Delta k$ (fine mesh) is needed.

To calculate the integrals in \eqnref{Calc_Wl,Calc_Wt} numerically the Simpson integration schema with equidistant mesh spacing is applied. This leads to an integration with small $\Delta k$ with a big $N$ which is computational not optimal with respect to efficiency. Since we calculate the wakefield usually just once in the initialization phase the overall performance will not be affected from this.

\section{CSR Wakefields}
The electromagnetic field due to a particle moving on a circle in free space can be calculated exactly with the Li\'enard-Wiechert potentials. The field has been calculated for all points on the same circle \cite{bib:schott,bib:schwinger1949}. For high particle energies the radiated power is almost exclusively emitted in forward direction, whereas for low energies a fraction is also emitted in transverse and backward direction. For the case of high-energetic particles an impedance in forward direction can be calculated \cite{bib:murphy1997}. The procedure is then the same as for a regular wakefield with the important difference that wakes exert forces on trailing particles only. The electromagnetic fields of a particle propagating on the mid-plane between two parallel metallic plates that stretch to infinity \cite{bib:schwinger1949} and for finite plates \cite{bib:nodvick1954} can also be calculated. For the infinite plates an impedance can be calculated \cite{bib:murphy1997}.

All of these approaches for CSR neglect any transient effects due to the finite length of the bend. Instead they describe the steady state case of a bunch circling infinitely long in the field of a dipole magnet. In \cite{bib:saldin1997} the four different stages of a bunch passing a bending magnet are treated separately and for each a corresponding wake function is derived. This model is used in \opalt for \keyword{1D-CSR}.

The 1-dimensional approach also neglects any influence of the transverse dimensions and of changes in current density between retarded and current time. On the other hand it gives a good approximation of effects due to CSR in short time.

In addition to the \keyword{1D-CSR} model also one that makes use of an integrated Green function  \cite{bib:mitchell2013}, \keyword{1D-CSR-IGF}.

\section{The \keywordinheader{WAKE} Command}
\label{sec:wakecmd}
The general input format is
\begin{example}
label:WAKE, TYPE=string, NBIN=real, CONST_LENGTH=bool,
      CONDUCT=string, Z0=real, FORM=string, RADIUS=real,
      SIGMA=real, TAU=real, FILTERS=string-array;
\end{example}
The format for a CSR wake is
\begin{example}
label:WAKE, TYPE=string, NBIN=real, FILTERS=string-array;
\end{example}

\begin{table}[ht] \footnotesize
  \begin{center}
    \caption{Wakefield command summary}
    \label{tab:wakefieldcmd}
    \begin{tabular}{|l|p{0.6\textwidth}|l|}
      \hline
      \tabhead{Command & Purpose}
      \hline
      \tabline[sec:wakefield]{WAKE}{Specify a wakefield}
      \tabline[sec:WTYPE]{TYPE}{\sloppy Specify the wake function [\keyword{1D-CSR}, \keyword{1D-CSR-IGF}, \keyword{LONG-SHORT-RANGE}, \keyword{TRANSV-SHORT-RANGE}, \keyword{LONG-TRANSV-SHORT-RANGE}]}
      \tabline[sec:NBIN]{NBIN}{Number of bins used in the calculation of the line density}
      \tabline[sec:CONSTLEN]{CONST\_LENGTH}{\keyword{TRUE} if the length of the bunch is considered to be constant}
      \tabline[sec:CONDUCT]{CONDUCT}{Conductivity [\keyword{AC}, \keyword{DC}]}
      \tabline[sec:Z]{Z0}{Impedance of the beam pipe in [$\Omega$]}
      \tabline[sec:FORM]{FORM}{The form of the beam pipe [\keyword{ROUND}]}
      \tabline[sec:RADIUS]{RADIUS}{The radius of the beam pipe in [m]}
      \tabline[sec:SIGMA]{SIGMA}{Material constant dependent on the beam pipe material in [$\Omega^{-1} m$]}
      \tabline[sec:TAU]{TAU}{Material constant dependent on the beam pipe material in [$s$]}
      \tabline[sec:WFNAME]{FNAME}{Specify a file that provides a wake function}
      \tabline[sec:FILTER]{FILTER}{The names of the filters that should be applied}
      \hline
    \end{tabular}
  \end{center}
\end{table}

\section{Define the Wakefield to be used}
\label{sec:wakefield}
\index{WAKE}
The \keyword{WAKE} command defines data for a wake function on an element \seesec{Element:common}.

\section{Define the wakefield type}
\label{sec:WTYPE}
\index{TYPE}
A {string value} \seesec{astring} to specify the wake function, either \keyword{1D-CSR}, \keyword{1D-CSR-IGF}, \keyword{LONG-SHORT-RANGE},
\keyword{TRANSV-SHORT-RANGE} or \keyword{LONG-TRANSV-SHORT-RANGE}.

\section{Define the number of bins}
\label{sec:NBIN}
\index{NBIN}
The number of bins used in the calculation of the line density.

\section{Define the bunch length to be constant}
\label{sec:CONSTLEN}
\index{CONST\_LENGTH}
With the \keyword{CONST\_LENGTH} flag the bunch length can be set to be constant. This has no effect on CSR wakefunctions.

\section{Define the conductivity}
\label{sec:CONDUCT}
\index{CONDUCTIVITY}
The conductivity of the bunch which can be set to either \keyword{AC} or \keyword{DC}. This has no effect on CSR wakefunctions.

\section{Define the impedance}
\label{sec:Z}
\index{Z0}
The impedance $Z_0$ of the beam pipe in [$\Omega$]. This has no effect on CSR wakefunctions.

\section{Define the form of the beam pipe}
\label{sec:FORM}
\index{FORM}
The form of the beam pipe can be set to \keyword{ROUND}. This has no effect on CSR wakefunctions.

\section{Define the radius of the beam pipe}
\label{sec:RADIUS}
\index{RADIUS}
The radius of the beam pipe in [m]. This has no effect on CSR wakefunctions.

\section{Define the \texorpdfstring{$\sigma$}{sigma} of the beam pipe}
\label{sec:SIGMA}
\index{SIGMA}
The $\sigma$ of the beam pipe (material constant), see \eqnref{dc_cond}. This has no effect on CSR wakefunctions.

\section{Define the relaxation time (\texorpdfstring{$\tau$}{tau}) of the beam pipe}
\label{sec:TAU}
\index{TAU}
The $\tau$ defines the relaxation time and is needed to calculate the impedance of the beam pipe \seeeqn{dc_cond}. This has no effect on CSR wakefunctions.

\section{Import a wakefield from a file}
\label{sec:WFNAME}
\index{FNAME}

Since we only need values of the wake function at several discreet points to calculate the force on the particle it is also possible to specify these in a file.To get required data points of the wakefield not provide in the file we linearly interpolate the available function values. The files are specified in the SDDS format \cite{bib:borland1995,bib:borland1998}.

Whenever a file is specified \opal will use the wakefield found in the file and ignore all other commands related to round beam pipes.

\section{List of Filters}
\label{sec:FILTER}
  Array of names of filters to be applied to the longitudinal histogram of the bunch to get rid of the noise and to calculate derivatives. All the filters are applied to the line density in the order they appear in the array. The last filter is also used for calculating the derivatives. The actual filters have to be defined elsewhere.


\section{The \keywordinheader{FILTER} Command}
Filters can be defined which then are applied to the line density of the bunch. The following smoothing filters are implemented: \keyword{Savitzky-Golay}, \keyword{Stencil}, \keyword{FixedFFTLowPass}, \keyword{RelativFFTLowPass}. The input format for them is
\begin{example}
label:FILTER, TYPE=string, NFREQ=real, THRESHOLD=real,
      NPOINTS=real, NLEFT=real, NRIGHT=real,
      POLYORDER=real
\end{example}
\begin{kdescription}
\item[TYPE]
  The type of filter: \keyword{Savitzky-Golay}, \keyword{Stencil}, \keyword{FixedFFTLowPass}, \keyword{RelativFFTLowPass}
\item[NFREQ]
  Only used in \keyword{FixedFFTLowPass}: the number of frequencies to keep
\item[THRESHOLD]
  Only used in \keyword{RelativeFFTLowPass}: the minimal strength of frequency compared to the strongest to consider.
\item[NPOINTS]
  Only used in \keyword{Savitzky-Golay}: width of moving window in number of points
\item[NLEFT]
  Only used in \keyword{Savitzky-Golay}: number of points to the left
\item[NRIGHT]
  Only used in \keyword{Savitzky-Golay}: number of points to the right
\item[POLYORDER]
  Only used in \keyword{Savitzky-Golay}: polynomial order to be used in least square approximation
\end{kdescription}
The \keyword{Savitzky-Golay} filter and the ones based on the FFT routine provide a derivative on a natural way. For the \keyword{Stencil} filter a second order stencil is used to calculate the derivative.

An implementation of the \keyword{Savitzky-Golay} filter can be found in the Numerical Recipes. The \keyword{Stencil} filter uses the following two stencil consecutively to smooth the line density:
$$f_i = \frac{7\cdot f_{i-4} + 24\cdot f_{i-2} + 34\cdot f_{i} + 24\cdot f_{i+2} + 7\cdot f_{i+4}}{96}$$
and
$$f_i = \frac{7\cdot f_{i-2} + 24\cdot f_{i-1} + 34\cdot f_{i} + 24\cdot f_{i+1} + 7\cdot f_{i+2}}{96}.$$
For the derivative a standard second order stencil is used:
$$f\primed_i = \frac{f_{i-2} - 8\cdot f_{i-1} + 8\cdot f_{i+1} - f_{i+2}}{h}$$
This filter was designed by Ilya Pogorelov for the ImpactT implementation of the CSR 1D model.

The FFT based smoothers calculate the Fourier coefficients of the line density. Then they set all coefficients corresponding to frequencies above a certain threshold to zero. Finally the back-transformation is calculate using this coefficients. The two filters differ in the way they identify coefficients which should be set to zero. \keyword{FixedFFTLowPass} uses the n lowest frequencies whereas \keyword{RelativeFFTLowPass} searches for the coefficient which has the biggest absolute value. All coefficients which, compared to this value, are below a threshold (measure in percents) are set to zero. For the derivative the coefficients are multiplied with the following function (this is equivalent to a convolution):
$$g_{i} =
\begin{cases}
i \frac{2\pi \imath}{N\cdot L} & i < N/2 \\
-i \frac{2\pi \imath}{N\cdot L} & i > N/2
\end{cases}$$
where $N$ is the total number of coefficients/sampling points and $L$ is the length of the bunch.

\input{footer}