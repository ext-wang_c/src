def p1 (0,0,0) 
def p2 (-4,0,0)
def p3 (-4,4,0) 
def p4 (0,4,0)
def p5 (0,0,4) 
def p6 (-4,0,4)
def p7 (-4,4,4) 
def p8 (0,4,4)

def dL1 1 def dL2 1
def straight_path1 dL1.[-2,0,-3]
def straight_path2 dL2.[-2,0,3]

def p9 (-2,2,0) 
def p10 (p9)+[straight_path1]
def p11 (-2,2,4) 
def p12 (p11)+[straight_path2]
def p13 (-2,4,0)-[0.005,0,0]
def p14 (-2,0,0)-[0.005,0,0]
def p15 (p14)+[straight_path1]-[0.075,0,0]
def p16 (p13)+[straight_path1]-[0.075,0,0]
def p17 (p9)+[straight_path1]-[0.075,0,0]
def p18 (p9)+0.75.[straight_path1]-[0.8*0.075,0,0]

def straight_path3 (p9)-(p18)
def p19 (p9) - 0.25.[straight_path3]

def p20 (p18)+[0.,0.2,-0.2]

def tilt rotate(15, (p9), [1,0,0])
def translation translate([-2,0,2])
def ds 1

def N 10
def rbend {
  put { [[translation]] } {
    polygon[cull=false, fill=green!50, fill opacity=0.5](p1)(p2)(p3)(p4)
    polygon[cull=false, fill=blue!20!red, fill opacity=0.5](p2)(p6)(p7)(p3)
    polygon[cull=false, fill opacity=0.](p6)(p5)(p8)(p7)
    polygon[fill=blue!20!red, fill opacity=0.3](p5)(p1)(p4)(p8)
    polygon[cull=false, fill=blue!20, fill opacity=0.5](p4)(p3)(p7)(p8)
    polygon[cull=false](p1)(p5)(p6)(p2)
    polygon[cull=false, fill=yellow!50, fill opacity=0.5, color=yellow!50, line width=2pt](p13)(p14)(p15)(p16)
    put { [[tilt]] }
      {
        %% design path %%
        line[arrows=-<, line width=2pt] (p9)(p10)
        line[arrows=->, line width=2pt] (p11)(p12)
        sweep[line width=2pt] 
          {N, rotate(-2 * atan2(2, 3)/N, (-5, 0, 2), [0, 1, 0]) }
          (p9)

      }
  }
}

def alpha {
     %% normal %%
  put { [[translation]] } {
     line[arrows=->,color=blue,line width=1pt] (-2,2,-2)(-2,2,-0.5)
     line[color=blue,line width=1pt](-2,2,-0.5)(p9)
     special |\path #1 node[left] {$\vec{n}$};
             | (-2,2,-1)

     %% alpha %%
     sweep[line width=1pt]  
     {N, rotate(atan2(2,3)/N, (p9), [0, 1, 0]) } (-2,2,-1.5)
     special |\path #1 node[right] {$\alpha$};|(-2.4,2,-2.15)
  }
}

def beta {
  put { [[translation]] } {
    line[color=blue, style=densely dotted, line width=1pt] (p9)(p19)
    line[arrows=<-,color=blue, style=densely dotted, line width=1pt] (p19)(p17)
    special |\path #1 node[below] {$\vec{n}_{\text{proj}}$};
             | (p18)

    sweep [line width=1pt]{N, rotate(-12/N, (p9), [-3,0,2])} (p18)
     %% normal %%
     special |\path #1 node[right] {$\beta$};|(p20)
  }
}

def uaxis {
  line [arrows=<-] (ds,-ds,0)(0,-ds,0)
  special |\path #1 node[left]  {$u$};
          | (ds,-ds,0)
}

def vaxis {
  line [arrows=->] (0,-ds,0)(0,0,0)
  special |\path #1 node[left]  {$v$};
          | (0,0,0)
}

def saxis {
  line [arrows=->] (0,-ds,0)(0,-ds,ds)
  special |\path #1 node[right]  {$s$};
          | (0,-ds,ds)
}

put { rotate(160, [0, 1, 0]) then rotate(20, [1, 0, 0]) then translate([-2, 9, 0])} 
          {{rbend}{saxis}{uaxis}{vaxis}{alpha}{beta}}

put { rotate(180, [0, 1, 0]) then rotate(90, [1, 0, 0]) then translate([0, 0, 0])  then scale(0.8)}
      {{rbend}{saxis}{uaxis}{alpha}}

put { rotate(-120, [0, 1, 0]) then rotate(5, [1,0,0]) then translate([5, -8, 0])  then scale(0.8)}
      {{rbend}{beta}
      put { translate([-5,0,-3]) } {
        {saxis}{vaxis}{uaxis}
      }}

global {language tikz}
