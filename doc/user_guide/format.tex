\input{header}

\chapter{Command Format}
\label{chp:format}
\index{Format!Commands}
\index{Command!Format|(}
All flavors of \opal using the same input language the \mad language. The language dialect here is
ajar to \madnine, for hard core \madeight users there is a conversion guide.

 It is the first time that
machines such as cyclotrons, proton and electron linacs can be described within the same language
in the same simulation framework.
\section{Statements and Comments}
\label{sec:statements}
\index{Statement}
\index{Comment}
Input for \opal is free format, and the line length is not limited.
During reading, input lines are normally printed on the echo file,
but this feature can be turned off for long input files.
The input is broken up into tokens (words, numbers, delimiters etc.),
which form a sequence of commands, also known as statements.
Each statement must be terminated by a semicolon (\texttt{;}),
and long statements can be continued on any number of input lines.
White space, like blank lines, spaces, tabs,
and newlines are ignored between tokens.
Comments can be introduced with two slashes (\texttt{//})
and any characters following the slashes on the same line are ignored.

The C~convention for comments (\texttt{/* ... */}) is also accepted.
The comment delimiters \texttt{/*} and \texttt{*/} can be nested;
this allows to ``comment out'' sections of input.

In the following descriptions,
words in \texttt{lower case} stand for syntactic units
which are to be replaced by actual text.
\texttt{UPPER CASE} is used for keywords or names.
These must be entered as shown.
Ellipses (\texttt{...}) are used to indicate repetition.

The general format for a command is
\begin{example}
keyword,attribute,...,attribute;
label:keyword,attribute,...,attribute;
\end{example}
It has three parts:
\begin{enumerate}
\item The \texttt{label} is required for a definition statement.
  \index{Command!Label}
  \index{Label!Command}
  Its must be an identifier \seesec{label} and gives a name to the
  stored command.
\item The \texttt{keyword} identifies the action desired.
  \index{Command!Keyword}
  \index{Keyword!Command}
  It must be an identifier \seesec{label}.
\item Each \texttt{attribute} is entered in one of the forms
\begin{example}
attribute-name
attribute-name=attribute-value
attribute-name:=attribute-value
\end{example}
and serves to define data for the command, where:
\begin{itemize}
\item The \texttt{attribute-name} selects the attribute,
  \index{Attribute!Name}
  \index{Name!Attribute}
  it must be an identifier \seesec{label}.
\item The \texttt{attribute-value} gives it a value \seesec{attribute}.
  \index{Attribute!Value}
  \index{Value!Attribute}
  When the attribute value is a constant or an expression preceded by
  the delimiter \texttt{=} it is evaluated immediately and the result
  is assigned to the attribute as a constant.
  When the attribute value is an expression preceded by the delimiter
  \texttt{:=} the expression is retained and re-evaluated whenever one
  of its operands changes.
\end{itemize}
Each attribute has a fixed attribute type \seesec{attribute}.\\
The \texttt{attribute-value} can only be left out for logical
attributes, this implies a \texttt{true} value.
\end{enumerate}

When a command has a \texttt{label},
\opal keeps the command in memory.
This allows repeated execution of the same command
by entering its label only:
\begin{example}
label;
\end{example}
or to re-execute the command with modified attributes:
\begin{example}
label,attribute,...,attribute;
\end{example}
If the label of such a command appears together with new attributes,
\opal makes a copy of the stored command, replaces the attributes entered,
and then executes the copy:
\begin{example}
QF:QUADRUPOLE,L=1,K1=0.01; // first definition of QF
QF,L=2;                    // redefinition of QF

MATCH;
...
LMD:LMDIF,CALLS=10;           // first execution of LMD
LMD;                          // re-execute LMD with
                              // the same attributes
LMD,CALLS=100,TOLERANCE=1E-5; // re-execute LMD with
                              // new attributes
ENDMATCH;
\end{example}

\section{Identifiers or Labels}
\label{sec:label}
\index{Label}
\index{Name}
\index{Identifier}
\index{Keyword}
An identifier refers to a keyword, an element, a beam line, a variable,
an array, etc.

A label begins with a letter, followed by an arbitrary number of letters,
digits, periods (\texttt{.}), underscores (\texttt{\_}).
Other special characters can be used in a label,
but the label must then be enclosed in single or double quotes.
It makes no difference which type of quotes is used,
as long as the same are used at either end.
The preferred form is double quotes.
The use of non-numeric characters is however strongly discouraged,
since it makes it difficult to subsequently process a \opal output with
another program.

When a name is not quoted, it is converted to upper case;
the resulting name must be unique.
An identifier can also be generated from a
string expression \seesec{astring}.

\section{Command Attribute Types}
\label{sec:attribute}
\index{Attribute!Value}
\index{Value!Attribute}
An object attribute is referred to by the syntax
\begin{example}
object-name->attribute-name
\end{example}
If the attribute is an array \seesec{anarray},
one of its components is found by the syntax
\begin{example}
object-name->attribute-name[index]
\end{example}
The following types of command attributes are available in \opal:
\begin{itemize}
\item String \seesec{astring},
\item Logical \seesec{alogical},
\item Real expression \seesec{areal},
\item Deferred expression \seesec{adefer},
\item Place \seesec{aplace},
\item Range \seesec{arange},
\item Constraint \seesec{aconstraint},
\item Variable Reference \seesec{areference}
\item Regular expression \seesec{wildcard}\ifthenelse{\boolean{ShowMap}}{,
\item Token list \seesec{toklist}}{}.
\item Array \seesec{anarray} of
  \begin{itemize}
  \item Logical \seesec{logarray},
  \item Real \seesec{realarray},
  \item String \seesec{strarray},
  \item Token lists \seesec{tokarray},
  \end{itemize}
\end{itemize}
See also:
\begin{itemize}
\item Operators \seetab{operator},
\item Functions \seetab{realfun},
\item Array functions \seetab{arrayfun},
\item Real functions of arrays \seetab{compfun},
\item Operand \seesec{operand},
\item Random generators \seesec{adefer}.
\end{itemize}

\section{String Attributes}
\label{sec:astring}
\index{String}
A string attribute makes alphanumeric information available,
e.g. a title, file name, element class name, or an option.
It can contain any characters, enclosed in single (\texttt{'})
or double (\texttt{"}) quotes.
However, if it contains a quote, this character must be doubled.
Strings can be concatenated using the \texttt{\&} operator \seetab{stroperator}.
An operand in a string can also use the
function \keyword{STRING} \seetab{stringfun}.
String values can occur in string arrays \seesec{anarray}.

\begin{table}[!htb] \footnotesize
   \begin{center}
    \caption{String Operator in \opal}
    \label{tab:stroperator}
    \begin{tabular}{|l|p{0.5\textwidth}|l|l|}
      \hline
      \tabhead{Operator & Meaning & result type & operand types}
      \hline
      \texttt{X \& Y} & concatenate the strings \texttt{X} and \texttt{Y}.
      String concatenations are always evaluated immediately when read. &
      string &string,string \\
      \hline
    \end{tabular}
  \end{center}
\end{table}

\begin{table}[!htb] \footnotesize
  \begin{center}
    \caption{String Function in \opal}
    \label{tab:stringfun}
    \begin{tabular}{|l|p{0.5\textwidth}|l|l|}
      \hline
      \tabhead{Function & Meaning & result type & argument type}
      \hline
      \keyword{STRING(X)} &
      return string representation of the value
      of the numeric expression \texttt{X} &
      string &real \\
      \hline
    \end{tabular}
  \end{center}
\end{table}
\par
\noindent Examples:
\begin{example}
TITLE,"This is a title for the program run ""test""";
CALL,FILE="save";

REAL X=1;
TWISS,LINE=LEP&STRING(X+1);
\end{example}
The second example converts the value of the expression ``X+1'' to a
string and appends it to ``LEP'', giving the string ``LEP2''.

\section{Logical Expressions}
\label{sec:alogical}
\index{Logical}
Many commands in \opal require the setting of logical values (flags)
to represent the on/off state of an option.
A logical value is represented by one of the values \keyword{TRUE}
or \keyword{FALSE}, or by a logical expression.
A logical expression can occur in logical arrays \seesec{logarray}.
\par
A logical expression has the same format and operator precedence as a
logical expression in C.
It is built from logical operators \seetab{logoperator} and logical
operands:
\begin{example}
relation      ::= "TRUE" |
                  "FALSE" |
                  real-expr rel-operator real-expr

rel-operator  ::= "==" | "!=" | "<" | ">" | ">=" | "<="

and-expr      ::= relation | and-expr "&&" relation

logical-expr  ::= and-expr | logical-expr "||" and-expr
\end{example}

\begin{table}[!htb] \footnotesize
  \begin{center}
    \caption{Logical Operators in \opal}
    \label{tab:logoperator}
    \begin{tabular}{|l|p{0.5\textwidth}|l|l|}
      \hline
      \tabhead{Operator & Meaning & result type & operand type}
      \hline
      \texttt{X $<$ Y} & true, if \texttt{X} is less than \texttt{Y} &
      logical &real,real \\
      \texttt{X $<=$ Y} & true, if \texttt{X} is not greater than \texttt{Y} &
      logical &real,real \\
      \texttt{X $>$ Y} & true, if \texttt{X} is greater than \texttt{Y} &
      logical &real,real \\
      \texttt{X $>=$ Y} & true, if \texttt{X} is not less than \texttt{Y} &
      logical &real,real \\
      \texttt{X $==$ Y} & true, if \texttt{X} is equal to \texttt{Y} &
      logical &real,real \\
      \texttt{X $!=$ Y} & true, if \texttt{X} is not equal to \texttt{Y} &
      logical &real,real \\
      \texttt{X \&\& Y} & true, if both \texttt{X} and \texttt{Y} are true &
      logical &logical,logical \\
      \texttt{X || Y} &
      true, if at least one of \texttt{X} and \texttt{Y} is true &
      logical &logical,logical \\
      \hline
    \end{tabular}
  \end{center}
\end{table}
\par
\noindent Example:
\begin{example}
OPTION,ECHO=TRUE; // output echo is desired
\end{example}
When a logical attribute is not entered,
its default value is always \keyword{FALSE}.
When only its name is entered, the value is set to \keyword{TRUE}:
\begin{example}
OPTION,ECHO;      // same as above
\end{example}
\noindent Example of a logical expression:
\begin{example}
X>10 && Y<20 || Z==15
\end{example}

\section{Real Expressions}
\label{sec:areal}
\index{Real}
To facilitate the definition of interdependent quantities,
any real value can be entered as an arithmetic expression.
When a value used in an expression is redefined by the user
or changed in a matching process,
the expression is re-evaluated.
Expression definitions may be entered in any order.
\opal evaluates them in the correct order before it performs
any computation.
At evaluation time all operands used must have values assigned.
A real expression can occur in real arrays \seesec{realarray}.

A real expression is built from operators \seetab{operator} and
operands \seesec{operand}:

\begin{footnotesize}
\begin{example}
real-ref  ::= real-variable |
              real-array "[" index "]" |
              object "->" real-attribute |
              object "->" real-array-attribute "[" index "]" |

table-ref ::= table "@" place "->" column-name

primary   ::= literal-constant |
              symbolic-constant |
              "#" |
              real-ref |
              table-ref |
              function-name "(" arguments ")" |
              (real-expression)

factor    ::= primary |
              factor "^" primary

term      ::= factor |
              term "*" factor |
              term "/" factor

real-expr ::= term |
              "+" term |
              "-" term |
              real-expr "+" term |
              real-expr "-" term |
\end{example}
\end{footnotesize}

It may contain functions \seetab{realfun},
Parentheses indicate operator precedence if required.
Constant sub-expressions are evaluated immediately,
and the result is stored as a constant.

\section{Operators}
\index{Expression!Operator}
\index{Operator}
An expression can be formed using operators \seetab{operator} and
functions \seetab{realfun}
acting on operands \seesec{operand}.

\begin{table}[!htb]  \footnotesize
  \begin{center}
    \caption{Real Operators in \opal}
    \label{tab:operator}
    \begin{tabular}{|l|p{0.5\textwidth}|l|l|}
      \hline
      \tabhead{Operator & Meaning & result type & operand type(s)}
      \hline
      \multicolumn{4}{|c|}{\textbf{Real operators with one operand}}\\
      \hline
      \texttt{+ X} & unary plus, returns \texttt{X} &
      real &real \\
      \texttt{- X} & unary minus, returns the negative of \texttt{X} &
      real &real \\
      \hline
      \multicolumn{4}{|c|}{\textbf{Real operators with two operands}} \\
      \hline
      \texttt{X + Y} & add \texttt{X} to \texttt{Y} &
      real & real,real \\
      \texttt{X - Y} & subtract \texttt{Y} from \texttt{X} &
      real & real,real \\
      \texttt{X * Y} & multiply \texttt{X} by \texttt{Y} &
      real &real,real \\
      \texttt{X / Y} & divide \texttt{X} by \texttt{Y} &
      real &real,real \\
      \texttt{X \^\ Y} &
      power, return \texttt{X} raised to the power \texttt{Y}
      ($\mathtt{Y} > 0$) & real &real,real \\
      \hline
    \end{tabular}
  \end{center}
\end{table}

\begin{table}[!htb] \footnotesize
  \begin{center}
    \caption{Real Functions in \opal}
    \label{tab:realfun}
    \begin{tabular}{|l|l|l|l|}
      \hline
      \tabhead{Function & Meaning & result type & argument type(s)}
      \hline
      \multicolumn{4}{|c|}{\textbf{Real functions with no arguments}} \\
      \hline%
      \keyword{RANF()} & random number, uniform distribution in [0,1) &
      real &- \\
      \keyword{GAUSS()} & random number, Gaussian distribution with $\mu=0$ and $\sigma=1$ &
      real &- \\
      \keyword{GETEKIN()} & returns the kinetic energy of the bunch (MeV) &
      real &- \\
      \keyword{USER0()} & random number, user-defined distribution &
      real &-
      \ifthenelse{\boolean{ShowMap}}{\\
      \keyword{SI()} &
      arc length from start of ring to the entry of the current element.
      This function is only available in the
      \keyword{EALIGN} command \seesec{erroralign} &
      real &- \\
      \keyword{SC()} &
      arc length from start of ring to the center of the current element.
      This function is only available in the
      \keyword{EALIGN} command \seesec{erroralign} &
      real &- \\
      \keyword{SO()} &
      arc length from start of ring to the exit of current the element.
      This function is only available in the
      \keyword{EALIGN} command \seesec{erroralign} &
      real &- }{}\\
      \hline
      \multicolumn{4}{|c|}{\textbf{Real functions with one argument}} \\
      \hline
      \keyword{TRUNC(X)} &
      truncate \texttt{X} towards zero (discard fractional part) &
      real &real \\
      \keyword{ROUND(X)} & round \texttt{X} to nearest integer &
      real &real \\
      \keyword{FLOOR(X)} & return largest integer not greater than \texttt{X} &
      real &real \\
      \keyword{CEIL(X)} & return smallest integer not less than \texttt{X} &
      real &real \\
      \keyword{SIGN(X)} & return sign of \texttt{X}
      (+1 for \texttt{X} positive, -1 for \texttt{X} negative,
      0 for \texttt{X} zero) & real &real \\
      \keyword{SQRT(X)} & return square root of \texttt{X} &
      real &real \\
      \keyword{LOG(X)} & return natural logarithm of \texttt{X} &
      real &real \\
      \keyword{EXP(X)} & return exponential to the base $e$ of \texttt{X} &
      real &real \\
      \keyword{SIN(X)} & return trigonometric sine of \texttt{X} &
      real &real \\
      \keyword{COS(X)} & return trigonometric cosine of \texttt{X} &
      real &real \\
      \keyword{ABS(X)} & return absolute value of \texttt{X} &
      real &real \\
      \keyword{TAN(X)} & return trigonometric tangent of \texttt{X} &
      real &real \\
      \keyword{ASIN(X)} & return inverse trigonometric sine of \texttt{X} &
      real &real \\
      \keyword{ACOS(X)} & return inverse trigonometric cosine of \texttt{X} &
      real &real \\
      \keyword{ATAN(X)} & return inverse trigonometric tangent of \texttt{X} &
      real &real \\
      \keyword{TGAUSS(X)} &
      random number, Gaussian distribution with $\sigma$=1,
      truncated at \texttt{X} &
      real &real \\
      \keyword{USER1(X)} &
      random number, user-defined distribution with one parameter &
      real &real \\
      \keyword{EVAL(X)} &
      evaluate the argument immediately and transmit it as a constant &
      real &real \\
      \hline
      \multicolumn{4}{|c|}{\textbf{Real functions with two arguments}} \\
      \hline
      \keyword{ATAN2(X,Y)} &
      return inverse trigonometric tangent of \texttt{Y/X} &
      real &real,real \\
      \keyword{MAX(X,Y)} & return the larger of \texttt{X}, \texttt{Y} &
      real &real,real \\
      \keyword{MIN(X,Y)} &
      return the smaller of \texttt{X}, \texttt{Y} &
      real &real,real \\
      \keyword{MOD(X,Y)} &
      return the largest value less than \texttt{Y}
      which differs from \texttt{X} by a multiple of \texttt{Y} &
      real &real,real \\
      \keyword{USER2(X,Y)} &
      random number, user-defined distribution with two parameters &
      real &real,real \\
      \hline
    \end{tabular}
  \end{center}
\end{table}

\begin{table}[!htb] \footnotesize
  \begin{center}
    \caption{Real Functions of Arrays in \opal}
    \label{tab:arrayfun}
    \begin{tabular}{|l|p{0.45\textwidth}|l|l|}
      \hline
      \tabhead{Function & Meaning & result type & operand type}
      \hline
      \keyword{VMAX(X,Y)} &
      return largest array component &real&real array\\
      \keyword{VMIN(X,Y)} &
      return smallest array component &real&real array\\
      \keyword{VRMS(X,Y)} &
      return rms value of an array &real&real array\\
      \keyword{VABSMAX(X,Y)} &
      return absolute largest array component &real&real array\\
      \hline
    \end{tabular}
  \end{center}
\end{table}

Care must be used when an ordinary expression contains a random generator.
It may be re-evaluated at unpredictable times, generating a new value.
However, the use of a random generator in an assignment expression is safe.
\noindent Examples:
\begin{example}
D:DRIFT,L=0.01*RANF();    // a drift space with rand. length,
                          // may change during execution.
REAL P=EVAL(0.001*TGAUSS(X));  // Evaluated once and stored as a constant.
\end{example}

\section{Operands in Expressions}
\label{sec:operand}
\index{Expression!Operand}
\index{Operand}
A real expression may contain the operands listed in the following
subsections.

\subsection{Literal Constants}
Numerical values are entered like FORTRAN constants.
Real values are accepted in INTEGER or REAL format.
The use of a decimal exponent, marked by the letter \texttt{D} or \texttt{E},
is permitted.

\noindent Examples:
\begin{example}
1, 10.35, 5E3, 314.1592E-2
\end{example}

\subsection{Symbolic constants}
\index{Expression!Constant}
\index{Constant}
\opal recognizes a few built-in
mathematical and physical constants \seetab{constant}.
Their names must not be used for user-defined labels.
Additional symbolic constants may be defined \seesec{constant} to
simplify their repeated use in statements and expressions.

\begin{table}[!htb] \footnotesize
  \begin{center}
    \caption{Predefined Symbolic Constants}
    \label{tab:constant}
    \begin{tabular}{|l|c|c|c|}
      \hline
      \tabhead{\opal name & Mathematical symbol & Value & Unit}
      \hline
      \keyword{PI} & $\pi$ & 3.1415926535898 & 1 \\
      \keyword{TWOPI} & $2 \pi$ & 6.2831853071796 & 1 \\
      \keyword{RADDEG} & $180/\pi$ & 57.295779513082 & rad/deg \\
      \keyword{DEGRAD} & $\pi/180$ & .017453292519943 & deg/rad \\
      \keyword{E} & $e$ & 2.7182818284590 & 1 \\
      \keyword{EMASS} & $m\_e$ & .51099906e-3 & GeV \\
      \keyword{PMASS} & $m\_p$ & .93827231 & GeV \\
      \keyword{HMMASS} &  $m\_{h^{-}}$ & .939277 & GeV \\
      \keyword{CMASS} & $m_c$ & 12*0.931494027 & GeV \\
      \keyword{UMASS} & $m_u$ & 238*0.931494027 & GeV \\
      \keyword{MMASS} & $m_\mu$ & 0.1057 & GeV \\
      \keyword{DMASS} & $m_d$ & 2*0.931494027 & GeV \\
      \keyword{XEMASS} & $m_{xe}$ &124*0.931494027 & GeV \\
      \keyword{CLIGHT} & $c$ & 299792458 & m/s \\
      \keyword{OPALVERSION} &  & 120 & for 1.2.0 \\
      \keyword{RANK} & & $0\ldots N_{p}-1$ & 1 \\
      \hline
    \end{tabular}
  \end{center}
\end{table}
Here the \keyword{RANK} represents the MPI-Rank of the process and $N_{p}$ the total number of MPI processes.
\subsection{Variable labels}
\label{sec:avariable}
\index{Variable}
Often a set of numerical values depends
on a common variable parameter.
Such a variable must be defined as a global
variable \seesec{variable} defined by one of
\begin{example}
REAL X=expression;
REAL X:=expression;
VECTOR X=vector-expression;
VECTOR X:=vector-expression;
\end{example}
When such a variable is used in an expression,
\opal uses the current value of the variable.
When the value is a constant or an expression preceded by the delimiter
\texttt{=} it is evaluated immediately and the result is assigned to the
variable as a constant.
When the value is an expression preceded by the delimiter \texttt{:=}
the expression is retained and re-evaluated whenever one of its operands
changes.\\
\noindent Example:
\begin{example}
REAL L=1.0;
REAL X:=L;
D1:DRIFT,L:=X;
D2:DRIFT,L:=2.0-X;
\end{example}
When the value of \texttt{X} is changed,
the lengths of the drift spaces are recalculated as
\texttt{X} and \texttt{2-X} respectively.

\subsection{Element or command attributes}
In arithmetic expressions the attributes of physical elements
or commands can occur as operands.
They are named respectively by
\begin{example}
element-name->attribute-name
command-name->attribute-name
\end{example}
If they are arrays, they are denoted by
\begin{example}
element-name->attribute-name[index]
command-name->attribute-name[index]
\end{example}
Values are assigned to attributes in element definitions or commands.

\noindent Example:
\begin{example}
D1:DRIFT,L=1.0;
D2:DRIFT,L=2.0-D1->L;
\end{example}
\texttt{D1->L} refers to the length \texttt{L} of the drift space \texttt{D1}.

\subsection{Deferred Expressions and Random Values}
\label{sec:adefer}
Definition of random machine imperfections requires evaluation
of expressions containing random functions.
These are not evaluated like other expressions before a command
begins execution, but sampled as required from the distributions
indicated when errors are generated.
Such an expression is known as a \textbf{deferred expression}.
Its value cannot occur as an operand in another expression.

\ifthenelse{\boolean{ShowMap}}{
\noindent Example:
\begin{example}
ERROR:EALIGN,CLASS=QUADRUPOLE,DX=SIGMA*GAUSS();
\end{example}
All elements in range are assigned independent random
displacements sampled from a Gaussian distribution
with standard deviation \keyword{SIGMA}.
The quantity \keyword{ERROR->DX} must not occur as an operand
in another expression.

\subsection{Table References}
\label{sec:acell}
Values can be extracted from a table with the syntax
\begin{example}
table-name "@" place "->" column-name
\end{example}
Here \texttt{table-name} denotes a table \seechp{tables},
\texttt{place} denotes a table row \seesec{aplace},
and \texttt{column-name} denotes the name of a column in the table.

\noindent Example:
\begin{example}
TWISS@#E->BETX
\end{example}
denotes the horizontal beta function at the end of table
\keyword{TWISS}.
}{}

\section{Element Selection}
\index{Selection Of Elements}
\index{Element!Selection}
\index{Place}
Many \opal commands allow for the possibility to process or display
a subset of the elements occurring in a beam line or sequence. This is not yet available in:
\noopalt and \noopalcycl.

\subsection{Element Selection}
\label{sec:aplace}
A \texttt{place} denotes a single element, or the position
\textbf{following} that element.
It can be specified by one of the choices
\begin{description}
\item[{object-name[index]}]
The name \verb'object-name' is the name of an element, line or sequence,
and the integer \texttt{index} is its occurrence count in the beam line.
If the element is unique, \texttt{[index]} can be omitted.
\item[{\#S}]
denotes the position before the first physical element in the \textbf{full}
beam line.
This position can also be written \texttt{\#0}.
\item[{\#E}]
denotes the position after the last physical element in the \textbf{full}
beam line.
\end{description}
Either form may be qualified by one or more beam line names,
as described by the formal syntax:
\begin{example}
place ::= element-name |
          element-name "[" integer "]" |
          "#S" |
          "#E" |
          line-name "::" place
\end{example}
An omitted index defaults to one.
\noindent Examples: assume the following definitions:
\begin{example}
M: MARKER;
S: LINE=(C,M,D);
L: LINE=(A,M,B,2*S,A,M,B);
   SURVEY,LINE=L
\end{example}
The line \texttt{L} is equivalent to the sequence of elements
\begin{example}
A,M,B,C,M,D,C,M,D,A,M,B
\end{example}
Some possible \texttt{place} definitions are:
\begin{description}
\item[{C[1]}]
The first occurrence of element \texttt{C}.
\item[\#S]
The beginning of the line \texttt{L}.
\item[{M[2]}]
The second marker \texttt{M} at top level of line \texttt{L},
i.~e. the marker between second \texttt{A} and the second \texttt{B}.
\item[\#E]
The end of line \texttt{L}
\item[{S[1]::M[1]}]
The marker \texttt{M} nested in the first occurrence of \texttt{S}.
\end{description}

\subsection{Range Selection}
\label{sec:arange}
\index{Element!Range}
\index{Range}
A \texttt{range} in a beam line \seesec{line} is selected
by the following syntax:
\begin{example}
range ::= place |
          place "/" place
\end{example}
This denotes the range of elements from the first\texttt{place} to
the second \texttt{place}. Both positions are included.
A few special cases are worth noting:
\begin{itemize}
\item
When \texttt{place1} refers to a \keyword{LINE} \seesec{line}.
the range starts at the \textbf{beginning} of this line.
\item
When \texttt{place2} refers to a \keyword{LINE} \seesec{line}.
the range ends at the \textbf{ending} of this line.
\item
When both \texttt{place} specifications refer to the same object,
then the second can be omitted.
In this case, and if \texttt{place} refers to a
\keyword{LINE} \seesec{line} the range contains the whole of the line.
\end{itemize}
\noindent Examples: Assume the following definitions:
\begin{example}
M: MARKER;
S: LINE=(C,M,D);
L: LINE=(A,M,B,2*S,A,M,B);
\end{example}
The line L is equivalent to the sequence of elements
\begin{example}
A,M,B,C,M,D,C,M,D,A,M,B
\end{example}
\noindent Examples for \texttt{range} selections:
\begin{description}
\item[\#S/\#E]
  The full range or \texttt{L}.
\item[{A[1]/A[2]}]
  \texttt{A[1]} through \texttt{A[2]}, both included.
\item[{S::M/S[2]::M}]
  From the marker \texttt{M} nested in the first occurrence of
  \texttt{S},
  to the marker \texttt{M} nested in the second occurrence of
  \texttt{S}.
\item[{S[1]/S[2]}]
  Entrance of first occurrence of \texttt{S} through
  exit of second occurrence of \texttt{S}.
\end{description}

\section{Constraints}
\label{sec:aconstraint}
\index{Constraint}
Please note this is not yet available in:
\noopalt and \noopalcycl.

In matching it is desired to specify equality constraints,
as well as lower and upper limits for a quantity.
\opal accepts the following form of constraints:
\begin{example}
constraint          ::= array-expr constraint-operator array-expr

constraint-operator ::= "==" | "<" | ">"
\end{example}

\section{Variable Names}
\label{sec:areference}
\index{Variable}
A variable name can have one of the formats:
\begin{example}
   variable name ::= real variable |
                     object"->"real attribute
\end{example}
The first format refers to the value of the
{global variable} \seesec{variable},
the second format refers to a named \texttt{attribute} of the named
\texttt{object}.
\texttt{object} can refer to an element or a command

\section{Regular Expressions}
\label{sec:wildcard}
\index{Regular Expression}
\index{Wildcard}
Some commands allow selection of items via a \texttt{regular-expression}.
Such a pattern string \textbf{must} be enclosed in single or double quotes;
and the case of letters is significant.
The meaning of special characters follows the standard UNIX usage:
utility:
\begin{description}
\item[.]
Stands for a single arbitrary character,
\item[{[letter...letter]}]
Stands for a single character occurring in the bracketed string,
\noindent Example: ``\texttt{[abc]}'' denotes the choice of one of
\texttt{a,b,c}.
\item[{[character-character]}]
Stands for a single character from a range of characters,
\noindent Example: ``\texttt{[a-zA-Z]}'' denotes the choice of any letter.
\item[*]
Allows zero or more repetitions of the preceding item,
\noindent Example: ``\texttt{[A-Z]*}'' denotes a string of zero or more
upper case letters.
\item[$\backslash$character]
Removes the special meaning of \texttt{character},
\noindent Example: ``\texttt{$\backslash$*}'' denotes a literal asterisk.
\end{description}
All other characters stand for themselves.
The pattern
\begin{example}
"[A-Za-z][A-Za-z0-9_']*"
\end{example}
illustrates all possible unquoted identifier formats \seesec{label}.
Since identifiers are converted to lower case,
after reading they will match the pattern
\begin{example}
"[a-z][a-z0-9_']*"
\end{example}
\noindent Examples for pattern use:
\begin{example}
SELECT,PATTERN="D.."
SAVE,PATTERN="K.*QD.*\.R1"
\end{example}
The first command selects all elements whose names have exactly three
characters and begin with the letter \texttt{D}.
The second command saves definitions beginning with the letter \texttt{K},
containing the string \texttt{QD}, and ending with the string \texttt{.R1}.
The two occurrences of \verb'.*' each stand for an arbitrary
number (including zero) of any character,
and the occurrence \verb'\.' stands for a literal period.

\ifthenelse{\boolean{ShowMap}}{
\section{Token List}
\label{sec:toklist}
\index{Token List}
In some special commands \keyword{LIST} \seesec{list} an attribute
cannot be parsed immediately, since some information may not yet be
available during parsing.
Such an attribute is entered as a ``token list'', and it is parsed
again when the information becomes available.
Token lists can occur in token list arrays \seesec{tokarray}.
This is not yet available in:
\noopalt and \noopalcycl.

\noindent Example:
\begin{example}
LIST,COLUMN={X:12:6,Y:12:6};
\end{example}
where \texttt{X:12:6} and \texttt{Y:12:6} are two token lists,
and \texttt{\{X:12:6,Y:12:6\}} is a token list array.
}{}

\section{Arrays}
\label{sec:anarray}
\index{Real!Array}
\index{Array!Real}
An attribute array is a set of values of the same
{attribute type} \seesec{attribute}.
Normally an array is entered as a list in braces:
\begin{example}
{value,...,value}
\end{example}
The list length is only limited by the available storage.
If the array has only one value, the braces (\texttt{{}}) can be omitted:
\begin{example}
value
\end{example}

\subsection{Logical Arrays}
\label{sec:logarray}
\index{Array!Logical}
\index{Logical!Array}
For the time being, logical arrays can only be given as a list.
The formal syntax is:
\begin{example}
logical-array ::= "{" logical-list "}"

logical-list  ::= logical-expr |
                  logical-list "," logical-expr
\end{example}
\par
\noindent Example:
\begin{example}
{true,true,a==b,false,x>y && y>z,true,false}
\end{example}

\subsection{Real Arrays}
\label{sec:realarray}
\index{Array!Real}
\index{Real!Array}
Real arrays have the following syntax:
\begin{footnotesize}
\begin{example}
array-ref     ::= array-variable |
                  object "->" array-attribute |

table-ref     ::= "ROW" "(" table "," place ")" |
                  "ROW" "(" table "," place "," column-list ")"
                  "COLUMN" "(" table "," column ")" |
                  "COLUMN" "(" table "," column "," range ")"

columns       ::= column |
                  "{" column-list "}"

column-list   ::= column |
                  column-list "," column

column        ::= string

real-list     ::= real-expr |
                  real-list "," real-expr

index-select  ::= integer |
                  integer "," integer |
                  integer "," integer "," integer

array-primary ::= "{" real-list "}" |
                  "TABLE" "(" index-select "," real-expr ")" |
                  array-ref |
                  table-ref |
                  array-function-name "(" arguments ")" |
                  (array-expression)

array-factor  ::= array-primary |
                  array-factor "^" array-primary

array-term    ::= array-factor |
                  array-term "*" array-factor |
                  array-term "/" array-factor

array-expr    ::= array-term |
                  "+" array-term |
                  "-" array-term |
                  array-expr "+" array-term |
                  array-expr "-" array-term |
\end{example}
\end{footnotesize}

\begin{table}[!htb] \footnotesize
  \begin{center}
    \caption{Real Array Functions in \opal (acting component-wise)}
    \label{tab:compfun}
    \begin{tabular}{|l|p{0.5\textwidth}|l|l|}
      \hline
      \tabhead{Function & Meaning & result type & argument type}
      \hline
      \keyword{TRUNC(X)} &
      truncate \texttt{X} towards zero (discard fractional part) &
      real array &real array \\
      \keyword{ROUND(X)} & round \texttt{X} to nearest integer &
      real array &real array \\
      \keyword{FLOOR(X)} & return largest integer not greater than \texttt{X} &
      real array &real array \\
      \keyword{CEIL(X)} & return smallest integer not less than \texttt{X} &
      real array &real array \\
      \keyword{SIGN(X)} & return sign of \texttt{X}
      (+1 for \texttt{X} positive, -1 for \texttt{X} negative,
      0 for \texttt{X} zero) & real array &real array \\
      \keyword{SQRT(X)} & return square root of \texttt{X} &
      real array &real array \\
      \keyword{LOG(X)} & return natural logarithm of \texttt{X} &
      real array &real array \\
      \keyword{EXP(X)} & return exponential to the base $e$ of \texttt{X} &
      real array &real array \\
      \keyword{SIN(X)} & return trigonometric sine of \texttt{X} &
      real array &real array \\
      \keyword{COS(X)} & return trigonometric cosine of \texttt{X} &
      real array &real array \\
      \keyword{ABS(X)} & return absolute value of \texttt{X} &
      real array &real array \\
      \keyword{TAN(X)} & return trigonometric tangent of \texttt{X} &
      real array &real array \\
      \keyword{ASIN(X)} & return inverse trigonometric sine of \texttt{X} &
      real array &real array \\
      \keyword{ACOS(X)} & return inverse trigonometric cosine of \texttt{X} &
      real array &real array \\
      \keyword{ATAN(X)} & return inverse trigonometric tangent of \texttt{X} &
      real array &real array \\
      \keyword{TGAUSS(X)} &
      random number, Gaussian distribution with $\sigma$=1,
      truncated at \texttt{X} &
      real array &real array \\
      \keyword{USER1(X)} &
      random number, user-defined distribution with one parameter &
      real array &real array \\
      \keyword{EVAL(X)} &
      evaluate the argument immediately and transmit it as a constant &
      real array &real array \\
      \hline
    \end{tabular}
  \end{center}
\end{table}
\noindent Example:
\begin{example}
{a,a+b,a+2*b}
\end{example}
There are also three functions allowing the generation of real arrays:
\begin{kdescription}
\item[TABLE]
  \index{TABLE}
  Generate an array of expressions:
\begin{example}
TABLE(n2,expression)    // implies
                        // TABLE(1:n2:1,expression)
TABLE(n1:n2,expression) // implies
                        // TABLE(n1:n2:1,expression)
TABLE(n1:n2:n3,expression)
\end{example}
  These expressions all generate an array with \texttt{n2} components.
  The components selected by \texttt{n1:n2:n3} are filled from the given
  \texttt{expression};
  a C pseudo-code for filling is
\begin{example}
int i;
for (i = n1; i <= n2; i += n3) a[i] = expression(i);
\end{example}
  In each generated expression the special character hash sign (\texttt{\#})
  is replaced by the current value of the index \texttt{i}.
  \par
  \noindent Example:
\begin{example}
// an array with 9 components, evaluates to
// {1,4,7,10,13}:
table(5:9:2,3*#+1) // equivalent to
                   // {0,0,0,0,16,0,22,0,28}
\end{example}
\item[ROW]
  \index{ROW}
  Generate a table row:
\begin{example}
ROW(table,place)             // implies all columns
ROW(table,place,column list)
\end{example}
  This generates an array containing the named (or all) columns in the
  selected place.
\item[COLUMN]
  \index{COLUMN}
  Generate a table column:
\begin{example}
COLUMN(table,column)         // implies all rows
COLUMN(table,column,range)
\end{example}
  This generates an array containing the selected (or all) rows of the
  named column.
\end{kdescription}

\subsection{String Arrays}
\label{sec:strarray}
\index{Array!String}
\index{String!Array}
String arrays can only be given as lists of single values.
For permissible values String values \seesec{astring}.
\par
\noindent Example:
\begin{example}
{A, "xyz", A & STRING(X)}
\end{example}

\subsection{Token List Arrays}
\label{sec:tokarray}
\index{Array!Token List}
\index{Token List!Array}
Token list arrays are always lists of single token lists.
\par
\noindent Example:
\begin{example}
{X:12:8,Y:12:8}
\end{example}

\index{Command!Format|)}

\input{footer}