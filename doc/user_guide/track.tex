\input{header}

\chapter{Tracking}
\label{chp:track}
\index{Tracking|(}

\begin{table}[ht] \footnotesize
  \begin{center}
    \caption{Commands accepted in Tracking Mode}
    \label{tab:trackcmd}
    \begin{tabular}{|p{0.3\textwidth}|p{0.6\textwidth}|}
      \hline
      \tabhead{Command & Purpose}
      \hline
      \tabline[sec:trackmode]{TRACK}{Enter tracking mode}
      \tabline[sec:trackmode]{LINE}{Label of \keyword{LINE} or \keyword{SEQUENCE}}
      \tabline[sec:trackmode]{BEAM}{Label of \keyword{BEAM}}
      \tabline[sec:trackmode]{T0}{Initial time}
      \tabline[sec:trackmode]{DT}{Array of time step sizes for tracking}
      \tabline[sec:trackmode]{MAXSTEPS}{Array of maximal number of time steps}
      \tabline[sec:trackmode]{ZSTART}{z-location [m], from where to run simulation}
      \tabline[sec:trackmode]{ZSTOP}{Array of z-location [m], after which the simulation switches to the next set of \keyword{DT}, \keyword{MAXSTEPS} and \keyword{ZSTOP}}
      \tabline[sec:trackmode]{STEPSPERTURN}{Number of time steps per revolution period}
      \tabline[sec:trackmode]{TIMEINTEGRATOR}{Defines the time integrator used in \opalcycl}
      \tabline[sec:variable]{name=expression}{Parameter relation}
%      \tabline[sec:tracknoise]{NOISE}{& Define power supply ripple}
      \ifthenelse{\boolean{ShowMap}}{\tabline[sec:trackstart]{START}{Define initial conditions}
      \tabline[sec:tracksave]{TSAVE}{Save end conditions}}{}
      \tabline[sec:trackrun]{RUN}{Run particles for specified number of turns or steps}
      \tabline[sec:trackmode]{ENDTRACK}{Leave tracking mode}
      \hline
    \end{tabular}
  \end{center}
\end{table}

\section{Track Mode}
\label{sec:trackmode}
\index{TRACK}
\index{ENDTRACK}

Before starting to track, a beam line \seesec{line} \ifthenelse{\boolean{ShowMap}}{or
sequence \seesec{sequence}}{} and a beam \seechp{beam} must be selected.
The time step (\keyword{DT}) and the maximal steps to track (\keyword{MAXSTEPS}) or \keyword{ZSTOP} should be set. This command causes \opal to enter ``tracking mode'',
in which it accepts only the track commands \seetab{trackcmd}. In order to preform several tracks, specify arrays of parameter
in \keyword{DT}, \keyword{MAXSTEPS} and \keyword{ZSTOP}. This can be used to change the time step manually.


The attributes of the command are:
\begin{kdescription}
\item[LINE]
  The label of a preceding \keyword[sec:line]{LINE} \seesec{line}
  \ifthenelse{\boolean{ShowMap}}{ or \keyword[sec:sequence]{SEQUENCE} \seesec{sequence}}{} (no default).
\item[BEAM]
  \sloppy The named \keyword{BEAM} command defines the particle mass, charge
  and reference momentum (default: \keyword{UNNAMED\_BEAM}).
  \index{UNNAMED\_BEAM}
\item[T0]
 The initial time [\si{\second}] of the simulation, its default value is 0.
\item[DT]
  Array of  time step sizes for tracking, default length of the array is 1 and its only value is \SI{1}{\pico\second}.
\item[MAXSTEPS]
  Array of maximal number of time steps, default length of the array is 1 and its only value is 10.
\item[ZSTART]
  Initial position of the reference particle along the reference trajectory, default position is \SI{0.0}{\meter}.
\item[ZSTOP]
  Array of z-locations [m], default length of the array is 1 and its only value is $1E6$ [m]. The simulation switches to the next set, $i+1$, of \keyword{DT}, \keyword{MAXSTEPS} and \keyword{ZSTOP} if either it has been tracking with the current set for more than $\text{\keyword{MAXSTEPS}}_i$ steps or the mean position has reached a z-position larger than $\text{\keyword{ZSTOP}}_i$. If set $i$ is the last set of the array then the simulation stops.

 \item[TIMEINTEGRATOR]
  Define the time integrator. Currently only available in \opalcycl.
  The valid options are \keyword{RK-4}, \keyword{LF-2} and \keyword{MTS}:
  \begin{kdescription}
    \item[RK-4] the fourth-order Runge-Kutta integrator. This is the default integrator for \opalcycl.
    \item[LF-2] the second-order Boris-Buneman (leapfrog-like) integrator.
      Currently, \keyword{LF-2} is only available for multi-particles with/without space charge.
      For single particle tracking and tune calculations, use the \keyword{RK-4} for the time being.
    \item[MTS] the multiple-time-stepping integrator.
    Considering that the space charge fields change much slower than the external fields in cyclotrons,
    the space charge can be calculated less  frequently than the external field interpolation, so as to reduce time to solution.
    The outer step (determined by \keyword{STEPSPERTURN}) is used to integrate  space charge effects.
    A constant number of sub-steps per outer step is used to query external fields and to move the particles.
    The number of sub-steps can be set with the option \keyword{MTSSUBSTEPS} and its default value is 1.
    When using this integrator, the input file has to be rewritten in the units of the outer step.
   For example, extracts of the input file suited for
    \keyword{LF-2} or \keyword{RK-4} read
\begin{example}
Option, PSDUMPFREQ=100;
Option, REPARTFREQ=20;
Option, SPTDUMPFREQ=50;
Option, VERSION=10600;
REAL turns=5;
REAL nstep=3000;
TRACK, LINE=l1, BEAM=beam1, MAXSTEPS=nstep*turns, STEPSPERTURN=nstep,
TIMEINTEGRATOR="LF-2";
    RUN, METHOD = "CYCLOTRON-T", BEAM=beam1, FIELDSOLVER=Fs1, DISTRIBUTION=Dist1;
ENDTRACK;
\end{example}
and should be transformed to
\begin{example}
Option, MTSSUBSTEPS=10;
Option, PSDUMPFREQ=10;
Option, REPARTFREQ=2;
Option, SPTDUMPFREQ=5;
Option, VERSION=10600;
REAL turns=5;
REAL nstep=300;
TRACK, LINE=l1, BEAM=beam1, MAXSTEPS=nstep*turns, STEPSPERTURN=nstep,
TIMEINTEGRATOR="MTS";
    RUN, METHOD = "CYCLOTRON-T", BEAM=beam1, FIELDSOLVER=Fs1, DISTRIBUTION=Dist1;
ENDTRACK;
\end{example}
In general all step quantities should be divided by MTSSUBSTEPS.

In our first experiments on PSI injector II cyclotron, simulations with reduced
space charge solving frequency by a factor of 10 lie still very close to the original solution.
How large \keyword{MTSSUBSTEPS} can be chosen of course depends on the importance of space charge effects.
  \end{kdescription}

\item[STEPSPERTURN]
  Number of time steps per revolution period. Only available for \opalcycl, default value is 720.

\end{kdescription}

\ifthenelse{\boolean{ShowMap}}{
In \opalt and \opalmap, the command format is:
\begin{example}
TRACK, LINE=name, BEAM=name, MAXSTEPS=value, DT=value;
\end{example}
}{}

In \opalcycl, instead of setting time step, the time steps per-turn should be set.
The command format is:
\begin{example}
TRACK, LINE=name, BEAM=name, MAXSTEPS=value,  STEPSPERTURN=value;
\end{example}

Particles are tracked in parallel i.e. the coordinates of all particles
are transformed at each beam element as it is reached.

\opal leaves \textbf{track mode} when it sees the command
\begin{example}
  ENDTRACK;
\end{example}




\subsection{Track a Random Machine} \label{sec:randmach}
This example shows how to track a {\em random} machine i.e. some
parameters are random variables. At the moment (Version 1.1.4) there seams to be a problem when
having random variables in the Distribution command.
\begin{example}
Option, SCAN=TRUE;
......

REAL I=0;
WHILE (I < 3) {

   REAL rv1:= (RANF()*4.7);
   REAL rv2:=0.0;
   REAL rv3:=0.0;
   REAL rv4:=0.0;
   REAL rv5:=0.0;

   Ppo: PepperPot, L=200.0E-6, ELEMEDGE=6.0E-3,
        R=1.0E-4, PITCH=0.5E-4, NHOLX=20, NHOLY=20,
        XSIZE=5.0E-3, YSIZE=5.0E-3, OUTFN="ppo.h5";

   Col: ECollimator, L=3.0E-3, ELEMEDGE=7.0E-3,
        XSIZE=7.5E-4, YSIZE=7.5E-4, OUTFN="Coll.h5";
   SP1: Solenoid, L=1.20, ELEMEDGE=-0.5315,
        FMAPFN="1T2.T7", KS=8.246e-05 + rv2;
   SP2: Solenoid, L=1.20, ELEMEDGE=-0.397,
        FMAPFN="1T3.T7", KS=1.615e-05 + rv3;
   SP3: Solenoid, L=1.20, ELEMEDGE=-0.267,
        FMAPFN="1T3.T7", KS=1.016e-05 + rv4;
   SP4: Solenoid, L=1.20, ELEMEDGE=-0.157,
        FMAPFN="1T3.T7", KS=4.750e-05 + rv5;
   SP5: Solenoid, L=1.20, ELEMEDGE=-0.047,
        FMAPFN="1T3.T7", KS=0.0;

   gun: RFCavity, L=0.013, VOLT=(-47.51437343 + rv1),
        FMAPFN="1T1.T7", ELEMEDGE=0.00,
        TYPE="STANDING", FREQ=1.0e-6;

   value,{I, rv1, rv2, rv3, rv4, rv5};

   l1: Line=(gun, Ppo, sp1, sp2, sp3, sp4, sp5);

   SELECT, Line=l1;
   TRACK, line=l1, beam=beam1, MAXSTEPS=500, DT=2.0e-13;
    RUN, method="PARALLEL-T", beam=beam1,
    fieldsolver=Fs1, distribution:=Dist1;
   ENDTRACK;

   SYSTEM,"mkdir -p scan0-" & STRING(I);
   SYSTEM,"mv scan-0.h5 scan-0.stat scan-0.lbal scan0-"
          & STRING(I);
   I=EVAL(I+1.0);
}
\end{example}


\section{Track Particles}
\label{sec:trackrun}
\index{RUN}

This command starts or continues the actual tracking:
\begin{example}
RUN, METHOD=string, FIELDSOLVER=label, DISTRIBUTION=label-vector, BEAM=label,
FILE=string, TURNS=integer, MBMODE=string, PARAMB=float,
BOUNDARYGEOMETRY=string, MULTIPACTING=logical, OBJECTIVES=string-vector;
\end{example}
The \texttt{RUN} command initialises tracking and uses the most recent
particle bunch for initial conditions.
The particle positions may be the result of previous tracking.

Its attributes are:
\begin{kdescription}
\item[METHOD]
  The name (a string, see \secref{astring}) of the tracking method to be used.
  For the time being the following methods are known:
  \begin{kdescription}
    \ifthenelse{\boolean{ShowMap}}{
    \item[THIN]
    All elements are treated a s thin lenses.
    This is the fastest of the known method which do not lump elements.
    }{}
    \item[PARALLEL-T]
    This method puts \opal in \opalt mode \seechp{opalt}.
    \item[CYCLOTRON-T]
    This method puts \opal in \opalcycl mode \seechp{opalcycl}.
    \item[STATISTICAL-ERRORS]
    This is a method to let \opal run multiple times in parallel while adding imperfections to alignment and other physical quantities.
  \end{kdescription}
  \item[FIELDSOLVER]
  The field solver to be used \seechp{fieldsolver}.

  \item[DISTRIBUTION]
  The particle distribution to be used \seechp{distribution}.

  \item[BEAM]
  The particle beam \seechp{beam} to be used is specified.

  \item[FILE]
  The name of the file to be written (default="\texttt{track}").
  \item[TURNS]
  The number of turns (integer) to be tracked (default: 1, namely single bunch).

  In \opalcycl, this parameter represents the number of bunches those will be injected into the cyclotron. In restart mode, the code
  firstly read an attribute $NumBunch$ from $.h5$ file which records how many bunches have already been injected. If $NumBunch$
  $<$ $TURNS$, the last $TURNS$$ -$ $NumBunch$ bunches will be injected in sequence by reading the initial distribution from $.h5$ file.

  \item[MBMODE]
  This defines which mode of multi-bunch runs. There are two options for it, namely, \texttt{AUTO} and \texttt{FORCE}.
  See \secref{opalcycl:MultiBunch} for their explanations in detail.

  For restarting run with \texttt{TURNS} larger than one, if the existing bunches of the read-in step is large than one,
  the mode is forcedly set to \texttt{FORCE}. Otherwise, it is forcedly set to \texttt{AUTO}.

  This argument is available for \opalcycl.

  \item[PARAMB]
   This is a control parameter to define when to start to transfer from single bunch to multi-bunches for \texttt{AUTO} mode (default: 5.0).

   This argument is only available for \texttt{AUTO} mode multi-bunch run in \opalcycl.

   \item[MULTIPACTING] \seechp{multpact}\TODO{Describe attribute}
   \item[OBJECTIVES] An array of column names from the \filename{.stat} file used in \keyword{STATISTICAL-ERRORS} to compute mean value and standard deviation across all runs.
\end{kdescription}
Example:
\begin{verbatim}
run, file="table", turns=5, mbmode="AUTO", paramb=10.0,
     method="CYCLOTRON-T", beam=beam1, fieldsolver=Fs1,
     distribution=Dist1;
\end{verbatim}

This command tracks 5 bunches in cyclotron and writes the results on file \texttt{table}.

\subsection{\keywordinheader{STATISTICAL-ERRORS}}
\label{ssec:statistical-errors}
\index{STATISTICAL-ERRORS}
This method can be used to quantify the effects of imperfections to alignment or other physical quantities such as e.g. the phase or the amplitude. It doesn't propagate the particles directly. Instead it scans through the input file and replaces all occurrences of \keyword[tab:realfun]{GAUSS} and \keyword[tab:realfun]{TGAUSS} with randomly generated values of appropriate distribution. Then one of the other methods, e.g. \keyword{PARALLEL-T} is called. These two steps are then repeated many times.

To use this method one has to specify the \keyword{METHOD} using the following form:
\begin{center}
\texttt{STATISTICAL-ERRORS(<track{\textunderscore}method>, <ncores>, <nruns>)},
\end{center}
\noindent where \texttt{<track{\textunderscore}method>} is the method that should track the particles, \texttt{<ncores>} is the number of cores used for a run and \texttt{<nruns>} is the number of individual runs that should be performed. \textbf{It should be noted that the total number of cores available has to be greater or equal to \texttt{ncores} + 1.} One core is needed to manage the distribution of tasks and to collect the results. The other cores are used to perform the simulations. If in total $N \times \texttt{ncores} + 1$ cores are available then $N$ individual runs are processed in parallel each using \texttt{ncores}.

\sloppy For each run of the method \keyword{STATISTICAL-ERRORS} a unique base name is generated of the form \filename{\%\%\%\%\%\%\%\%-\%\%\%\%-\%\%\%\%}. Each individual run is then performed in a directory \filename{\%\%\%\%\%\%\%\%-\%\%\%\%-\%\%\%\%{\textunderscore}run{\textunderscore}ddddd}. The files that are produced by the \texttt{<track{\textunderscore}method>} are kept. \textbf{This can lead to a large amount of data especially when snapshots of the phase space are stored frequently. The user should make sure that the file system can handle the amount of data or set the option \keyword[sec:option]{PSDUMPFREQ} to a big number.}

In the end the method \keyword{STATISTICAL-ERRORS} computes the mean and the standard deviation for each variable in the array \keyword{OBJECTIVES} along the machine and stores this information in to the \filename{.stat} file.
\input{footer}