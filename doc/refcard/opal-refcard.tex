%% objective - reference card for the \opal eigenvalue computation code
%% objective - list and explain \opal code usage
%% modified - 2008 jun 10, creation from latex cheat sheet file, benedikt oswald
%%
%%

\documentclass[10pt,landscape,a4paper]{article}
\usepackage{color}
\usepackage{multicol}
\usepackage{calc}
\usepackage{ifthen}

\usepackage{amsmath}         %% diverse Matheerweiterungen
\usepackage{amssymb}         %% diverse Matheerweiterungen, z.B. \mathbb{R}
\usepackage{wasysym}         %% diverse Matheerweiterungen, z.B. \oiint
\usepackage{epsfig}          %% um eps-Dateien einzubinden (\epsfig{file=...})
\usepackage{longtable}       %% fuer Tabellen ueber mehrere Seiten

\usepackage{graphicx}
\usepackage{makeidx}

\usepackage{natbib}        

\usepackage[landscape]{geometry} 
% If you're reading this, be prepared for confusion.  Making this was
% a learning experience for me, and it shows.  Much of the placement
% was hacked in; if you make it better, let me know...


% 2008-04
% Changed page margin code to use the geometry package. Also added code for
% conditional page margins, depending on paper size. Thanks to Uwe Ziegenhagen
% for the suggestions.

% 2006-08
% Made changes based on suggestions from Gene Cooperman. <gene at ccs.neu.edu>


% To Do:
% \listoffigures \listoftables
% \setcounter{secnumdepth}{0}


% This sets page margins to .5 inch if using letter paper, and to 1cm
% if using A4 paper. (This probably isn't strictly necessary.)
% If using another size paper, use default 1cm margins.
\ifthenelse{\lengthtest { \paperwidth = 11in}}
	{ \geometry{top=.5in,left=.5in,right=.5in,bottom=.5in} }
	{\ifthenelse{ \lengthtest{ \paperwidth = 297mm}}
		{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
		{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
	}

% Turn off header and footer
\pagestyle{empty}
\bibliographystyle{plain}

% Redefine section commands to use less space
\makeatletter
\renewcommand{\section}{\@startsection{section}{1}{0mm}%
                                {-1ex plus -.5ex minus -.2ex}%
                                {0.5ex plus .2ex}%x
                                {\normalfont\large\bfseries}}
\renewcommand{\subsection}{\@startsection{subsection}{2}{0mm}%
                                {-1explus -.5ex minus -.2ex}%
                                {0.5ex plus .2ex}%
                                {\normalfont\normalsize\bfseries}}
\renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{0mm}%
                                {-1ex plus -.5ex minus -.2ex}%
                                {1ex plus .2ex}%
                                {\normalfont\small\bfseries}}


\newcommand{\opal}{\textsc{OPAL }}
\newcommand{\opalt}{\textsc{OPAL-t }}
\newcommand{\opalcycl}{\textsc{OPAL-cycl }}
\newcommand{\opalmap}{\textsc{OPAL-map }}

\newcommand{\noopalt}{\textsc{\leftthumbsdown \opalt }}
\newcommand{\noopalmap}{\textsc{\leftthumbsdown \opal }}
\newcommand{\noopalcycl}{\textsc{\leftthumbsdown \opalcycl }}




\makeatother

% Define BibTeX command
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}

% Don't print section numbers
\setcounter{secnumdepth}{0}


\setlength{\parindent}{0pt}
\setlength{\parskip}{0pt plus 0.5ex}


%% -----------------------------------------------------------------------
%% begin \opal refcard
%% -----------------------------------------------------------------------


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% New commands.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\unit}[1]{\mbox{$\mathrm{#1}$}}          %% physical unit
\renewcommand{\vec}[1]{\mathbf{#1}}                  %% vector
\newcommand{\uvec}[1]{\mathbf{\hat{#1}}}             %% unit vector
\newcommand{\mat}[1]{\mathsf{#1}}                    %% matrix
\newcommand{\func}[1]{\mbox{$\mathrm{#1}$}}          %% math. function
\newcommand{\cu}{i}                                  %% complex unit
%% replace this with an argument of resp. type.
\newcommand{\replace}[1]{\texttt{\mbox{<\textsl{#1}>}}} 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Colors.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\definecolor{CmdBoxBackground}{gray}{0.7}
\definecolor{ExampleBoxBackground}{gray}{0.9}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

%  \raggedright
\footnotesize
\begin{multicols}{3}


% multicol parameters
% These lengths are set only within the two main columns
%\setlength{\columnseprule}{0.25pt}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}





\begin{center}
     \LARGE{\large{\textbf{\textsf{\opal}}} reference card} \\
\end{center}


\begin{center}
  \includegraphics[width=70mm]{./pictures/torus200kmode0electricfield}
%   We have computed a few modes of a toroidal resonator structure, with
%   respective diameters of $1.0$ and $0.5$ meters. The plot shows the magnitude
%   of the electric field of the lowest mode with a resonance frequency
%   of $218.067+08$ Mhz and the associated quality factor $Q = 62'870e$ [-].
%   Mesh of the torus courtesy Markus Bopp, PSI.
\end{center}









\textsf{\textbf{Applicability:}} Commands in this reference card
apply to the \textsf{\opal} eigenvalue code only. The electromagnetic
frontend \textsf{heronion} is documented elsewhere.
%%
%%
%%
\textsf{\textbf{Iron Rule:}} parameters that are used for the solution of
a specific electromagnetic problem are communicated to the code
at one single location only, i.e. as command line parameters when
the solver is run.
%%
%%
\textsf{\textbf{Units:}} the \textsf{\opal} code uses the MKS system,
aka. meter, kilogram, second, for all its computations.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\textsf{Usage Paradigm}}


\texttt{\opal} is an advanced computational electrodynamics package consisting of three different,
subsequently used programs.
%%
%%
This document describes code usage. For detailed information on implementational questions
studying the code is essential and the documentation generated by Doxygen is helpful.
%%
%%
%%








%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\textsf{Physics}}

The \textsf{\opal} code computes the eigenmodal fields of electromagnetic
resonators by solving the electric field vector wave (\textit{curl-curl})
equation in $3$-dimensional space, in frequency domain and on a tetrahedral
mesh for scalar, non-dispersive dielectric and magnetic materials.
%%
%%
\begin{equation}
\label{eq_electric_curl_curl}
%%
\nabla \times \frac{1}{\mu_{r}} \nabla \times \vec{E}
%%
+ k_{0}^2 \epsilon_{r} \vec{E}
%%
= 0
%%
\end{equation}
%%
At present materials do not model electromagnetic losses. The computation
of a resonator's quality figure uses an \textit{a posteriori}
perturbation approach.
%%
%%
The eigenvalue $\ell = k_{0}^2$ is defined in terms of electromagnetic constants
and the resonance frequency $f_{\mathrm{res}}$.
%%
%%
The resonance frequency $f_{\mathrm{res}}$ is computed from the eigenvalue $\ell$
%%
%%
\begin{equation}
  \label{eq_fres_from_k0}
  f = \Biggl( \sqrt{\frac{\ell}{\epsilon_{0}\mu_{0}}} \: \: \Biggr) \frac{1}{2\pi}
\end{equation}
%%
%%
The eigenvalue shift $\sigma$ can also be understood as a frequency shift,
using Eq. (\ref{eq_fres_from_k0}), cf. also the \texttt{--sigma} parameter.
%%
The theory and the implementation of \textsf{\opal} is explained
in \cite[]{arbenzetal2001,geus2002}.











%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\textsf{Command Line Parameters}}






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{General}
\label{section_general}

The executable of the \textsf{\opal} code is \texttt{\opal\_driver}. Running the code
depends on the operating system.


\vspace{1mm}

\fbox{
\texttt{\opal\_driver}
}

\vspace{1mm}


\noindent \textbf{\textsf{Nota bene:}} On a Linux system, it is common
practice to use the \texttt{mpirun} command, e.g.

\vspace{1mm}

\noindent \texttt{mpirun -np 2 <path\_to\_\opal\_executable>/\opal\_driver ...}

\vspace{1mm}

The situation is different on Cray or IBM/bluegene systems.










%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Help}
\label{section_help}

To request a description of all built-in parameters of \textsf{\opal}
use


\vspace{1mm}

\fbox{
\texttt{--help}
}

\vspace{1mm}

\noindent \textbf{\textsf{Example:}} \texttt{\opal\_driver --help}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Tetrahedral mesh file}
\label{section_tetrahedral_mesh_file}

To specify the path to the file that contains the input tetrahedral volume 
and triangular surface mesh for the computation:


\vspace{1mm}

\fbox{
\texttt{--mesh=\replace{string}}
}


\vspace{2mm}

\noindent \textbf{\textsf{Example:}} \texttt{--mesh=cavity.h5}

\vspace{2mm}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Eigenvalue shift}
\label{section_eigenvalue_shift}

To specify the eigenvalue shift $\sigma$ used in the 
iterative solution of the eigenvalue computation


\vspace{1mm}

\fbox{
\texttt{--sigma=\replace{double}}
}


\vspace{2mm}

\noindent \textbf{\textsf{Example:}} \texttt{--sigma=2.0}

\vspace{2mm}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Number of computed eigenmodes}
\label{section_numbero_of_computed_eigenmodes}

To specify the desired number of eigenmodal solution (eigenmodes)
to be computed, specify:


\vspace{1mm}

\fbox{
\texttt{--kmax=\replace{integer}}
}


\vspace{2mm}

\noindent \textbf{\textsf{Example:}} \texttt{--kmax=10}

\vspace{2mm}

\noindent \textbf{\textsf{Nota bene:}} the \textsf{\opal} code
uses an iterative scheme to compute eigenmodes, i.e. a variable
number of iterations is required to obtain an eigenmode with
given accuracy (cf. parameter \texttt{--tol} parameter). By default
$5$ eigenmodes are computed for which the default number of
$200$iterations (cf. parameter \texttt{--jitmax}) is sufficient.
%%
%%
If more eigensolutions are required, then  \texttt{--jitmax} should
be increased. The increase depends on mesh quality,  complexity
of the problem and the number of desired modes.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Numerical precision}
\label{section_numerical_precision}

To specify the accuracy for the computation of 
the eigenmodes, write


\vspace{1mm}

\fbox{
\texttt{--tol=\replace{double}} ($1.0 \cdot 10^{-8}$)
}


\vspace{2mm}

\noindent \textbf{\textsf{Example:}} \texttt{--tol=1.0e-6} which is often
good enough or a good start to explore the eigenmodal space of a resonator
structure.

\vspace{2mm}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Finite element approximation order}
\label{section_finite_element_approximation_order}


At present $1^{\mathrm{st}}$ and $2^{\mathrm{nd}}$ order
base functions are available in \textsf{\opal}.
%%
%%
To select either of them, write


\vspace{1mm}

\fbox{
\texttt{--order=1}
}

\vspace{2mm}

\noindent or

\vspace{2mm}

\fbox{
\texttt{--order=2}
}

\vspace{2mm}

\noindent \textbf{\textsf{Caveat:}} While $1^{\mathrm{st}}$
order base functions are robust and reliable, computations
using $2^{\mathrm{nd}}$ order base functions may depend
on geometry and the calculation may not converge.

\vspace{2mm}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Preconditioner selection}
\label{section_preconditioner_selection}

\textsf{\opal} offers a choice of different preconditioners,
used in the iterative solution of the eigenmodal system.
%%
%%
Possible selections are, where \texttt{|} denotes the logical \texttt{OR}


\vspace{1mm}

\fbox{
\texttt{--aprec=ml | neumann | lu | diag | 2level | if } (ml)
}


\vspace{2mm}

\noindent \textbf{\textsf{Available preconditioner types:}} 
	
\noindent \texttt{--aprec=neumann} : requests a Neumann type preconditioner

\noindent \texttt{--aprec=ml} : requests a multilevel type preconditioner

\noindent \texttt{--aprec=lu} : requests a LU type preconditioner

\noindent \texttt{--aprec=2level} : requests a 2level type preconditioner

\noindent \texttt{--aprec=diag} : requests a diagonal type preconditioner

\noindent \texttt{--aprec=if} : requests a if type preconditioner


\vspace{2mm}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Switch off log file}
\label{section_switch_of_log_file}

By default, a \textsf{\opal} run produces a log file with information
on all aspects of the eigenmodal computation. To switch log file generation
off, write


\vspace{1mm}

\fbox{
\texttt{--no-logfile}
}


\vspace{2mm}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Symmetry plane configuration}
\label{section_symmetry_plane_configuration}

Often, the geometry of electromagnetic resonators exhibits symmetries which can
be exploited to reduce the computational effort considerably.
%%
A computational mesh in \textsf{\opal} compliand HDF5 file also contains
a list of surface triangles, each of which is attributed a surface tag whose
values are either $1,2$ or $3$.
%%
%%
The surface triangle tags, $1$, $2$ or $3$, denote the lowest three bits in a binary number;
depending on the bits being either set ($1$) or not set ($0$),
the resulting binary number can express decimal values in the range of
$0$ through $7$.
%%
%%
If the corresponding bit of a surface triangle tag is set, then triangles with this
tag are assigned the perfect electric conductor (PEC) boundary condition.
%%
If the bit is not set, then triangles with this tag are assigned the perfect
magnetic conductor (PMC) boundary condition, in short:

\noindent Bit $=0$ : $\vec{E} \times \vec{n} = 0$ (PEC)

\noindent Bit $=1$ : $\vec{E} \cdot \vec{n} = 0$ (PMC)


\vspace{1mm}

\fbox{
\texttt{--sym-plane-config=\replace{integer}}
}


\vspace{2mm}

\noindent \textbf{\textsf{Example:}} \texttt{--sym-plane-config=5}
%%
means that on surface triangles with
tag $1$ we have a PMC boundary condition, on surface triangles with tag $2$ we
have a PEC boundary condition, and on surface triangles with surface tag $3$ we
have, once again, a PMC boundary condition.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Debug mode}
\label{section_debug_mode}

To switch on debug mode during code execution, use


\vspace{1mm}

\fbox{
\texttt{--debug}
}


\vspace{2mm}










%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Result file}
\label{section_result_file}

To specify the name of the HDF5 file into which the eigenmodal
calculations' results are written


\vspace{1mm}

\fbox{
\texttt{--eigendatafile=\replace{string}}
}


\vspace{2mm}

\noindent \textbf{\textsf{Example:}} \texttt{--eigendatafile=box\_output.h5}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Cartesian sampling}
\label{section_result_file}

To specify an orthogonal cartesian region within which
the eigenmodal solution is sampled, use

\vspace{1mm}

\begin{verbatim}
--cartesian-sampling-x-start = <double>
--cartesian-sampling-x-stop  = <double>
--number-samples-x-axis      = <integer>   
--cartesian-sampling-y-start = <double>        
--cartesian-sampling-y-stop  = <double>
--number-samples-y-axis      = <integer>
--cartesian-sampling-z-start = <double>
--cartesian-sampling-z-stop  = <double>
--number-samples-z-axis      = <integer>
\end{verbatim}

\vspace{2mm}

\noindent \textbf{\textsf{Example:}}

\begin{verbatim}
--cartesian-sampling-x-start = 0.0
--cartesian-sampling-x-stop  = 1.0
--number-samples-x-axis      = 11
--cartesian-sampling-y-start = 0.5    
--cartesian-sampling-y-stop  = 1.5
--number-samples-y-axis      = 21
--cartesian-sampling-z-start = -1.5
--cartesian-sampling-z-stop  = -0.5
--number-samples-z-axis      = 41
\end{verbatim}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Cylinder sampling}
\label{section_result_file}

Specifying an orthogonal cyliner region within which
the eigenmodal solution is sampled, is similar to the
cartesian region selection. The cylindric region is
defined through the axis of the sampling cylinder volume
and the end position of its radius vector, w.r.t cylinder
axis' start location.



\begin{verbatim}
--cylinder-sampling-x-bottom                 = <double>
--cylinder-sampling-x-top                    = <double>
--cylinder-sampling-y-bottom                 = <double>   
--cylinder-sampling-y-top                    = <double>        
--cylinder-sampling-z-bottom                 = <double>
--cylinder-sampling-z-top                    = <double>
--number-samples-on-cylinder-axis            = <integer>
--cylinder-radial-vector-end-position-x      = <double>
--cylinder-radial-vector-end-position-y      = <double>
--cylinder-radial-vector-end-position-z      = <double>
--number-samples-on-cylinder-radial-vector   = <integer>
--number-samples-in-cylinder-azimuthal-plane = <integer>
\end{verbatim}

\vspace{2mm}

\noindent \textbf{\textsf{Example:}}

\begin{verbatim}
--cylinder-sampling-x-bottom                 = 0.5
--cylinder-sampling-x-top                    = 0.5
--cylinder-sampling-y-bottom                 = 0.7
--cylinder-sampling-y-top                    = 0.7
--cylinder-sampling-z-bottom                 = 0.0
--cylinder-sampling-z-top                    = 1.6
--number-samples-on-cylinder-axis            = 171
--cylinder-radial-vector-end-position-x      = 1.0
--cylinder-radial-vector-end-position-y      = 1.3
--cylinder-radial-vector-end-position-z      = 0.0
--number-samples-on-cylinder-radial-vector   = 11
--number-samples-in-cylinder-azimuthal-plane = 4
\end{verbatim}

\noindent which specifies a cylinder sampling volume where the cylinder's
axis starts at $[x=0.5,y=0.7,z=0.0]$ and ends at $[x=0.5,y=0.7,z=1.6]$ and
the end position of th radius vector is $[x=1.0,y=1.3,z=0.0]$ with
as many as $[171,11,4]$ samples on the cylinder axis, the radius vector
and on the cylinder azimuthal plane, respectively.


{\scriptsize
\bibliography{opal-ref-card-bibliography}
}

\vspace{2mm}

\scriptsize
%%
%%
For ontacts:
%%
\texttt{andreas.adelmann@psi.ch}.
%%
%%
\noindent \textbf{DISCLAIMER} All data in this reference card have been collected and presented
with great care. However, no responsibility is accepted for any consequences resulting
from errors or omissions. Neither do we take any responsibility for using the codes.
%%
%%
We encourage you to report bugs, problems and suggestions to us. 
%%
%%
\noindent On the internet type \texttt{www.amas.web.psi.ch} to find us.
%%
\end{multicols}
\end{document}















