Thesis
======

Here is the implementation portion of my CSE master thesis for ETH Zürich.  A detailed 
explanation of the theory behind the optimization method presented here can be found in 
Andrew Foster's thesis (link is dead: ~~http://n.ethz.ch/~anforste/Thesis.pdf~~).
